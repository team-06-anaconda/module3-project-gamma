from common.client import Queries
from accounts.models import Account, SessionOut
from bson.objectid import ObjectId


class SessionQueries(Queries):
    DB_NAME = "auth"
    COLLECTION = "sessions"

    def get(self, jti: str):
        return self.collection.find_one({"jti": jti})

    def create(self, jti: str, account: Account):
        result = self.collection.insert_one(
            {
                "jti": jti,
                "account_id": ObjectId(account.id)
            }
        )
        if result and result.inserted_id:
            return SessionOut(jti=jti, account_id=account.id)
        return None

    def delete(self, jti: str):
        self.collection.delete_many({"jti": jti})
