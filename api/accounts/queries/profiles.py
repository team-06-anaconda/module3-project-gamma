from common.client import Queries
from accounts.models import ProfileOut, ProfileIn, Profile


class ProfileRepository(Queries):
    DB_NAME = "auth"
    COLLECTION = "profiles"

    def update(self, user_id: str, profile: ProfileIn):
        query = ({"user_id": user_id})
        profile_input = profile.dict()
        print(profile_input)
        edits = {}
        for property in profile_input:
            if profile_input[property] or property == "can_fly_international":
                edits[property] = profile_input[property]

        if "birthday" in edits:
            edits["birthday"] = str(edits["birthday"])

        self.collection.update_one(query, {"$set": edits})
        updated_profile = self.collection.find_one(query)
        return ProfileOut(**updated_profile)

    def create(self, user_id: str):
        user_id = {"user_id": user_id}
        new_profile = Profile(**user_id).dict()

        #  defaults for special data types, avoids errors
        #  when communicating with front end.
        defaults = {"can_fly_international": False, "birthday": "2023-07-08"}
        new_profile.update(defaults)
        self.collection.insert_one(new_profile)
        return ProfileOut(**new_profile)

    def get(self, user_id: str):
        query = {"user_id": user_id}
        profile = self.collection.find_one(query)
        return ProfileOut(**profile)
