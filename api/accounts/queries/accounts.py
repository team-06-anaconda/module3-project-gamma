from common.client import Queries
from accounts.models import AccountIn, Account


class AccountRepo(Queries):
    DB_NAME = "auth"
    COLLECTION = "accounts"

    def create(self, account: AccountIn, hashed_password: str):
        data = account.dict()
        data["hashed_password"] = hashed_password
        del data["password"]
        del data["password2"]
        account = self.get(data["email"])
        if account:
            raise DuplicateAccountError

        self.collection.insert_one(data)
        data["id"] = str(data["_id"])
        return Account(**data)

    def get(self, email: str):
        data = self.collection.find_one({"email": email})
        if not data:
            return None
        data["id"] = str(data["_id"])
        return Account(**data)


class DuplicateAccountError(BaseException):
    pass
