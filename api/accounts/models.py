from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token
from datetime import date


class Account(BaseModel):
    id: str
    email: str
    hashed_password: str


class AccountIn(BaseModel):
    password: str
    password2: str
    email: str


class AccountOut(BaseModel):
    id: str
    email: str


class SessionOut(BaseModel):
    jti: str
    account_id: str


class AccountToken(Token):
    account: AccountOut


class AccountForm(BaseModel):
    username: str
    password: str


class HttpError(BaseModel):
    detail: str


class Profile(BaseModel):
    user_id: str
    nearest_airport: str | None
    can_fly_international: bool | None
    first_name: str | None
    last_name: str | None
    birthday: date | None
    picture_url: str | None
    banner_image_url: str | None
    preferred_gender: str | None
    about_me: str | None


class ProfileIn(BaseModel):
    nearest_airport: str | None
    can_fly_international: bool | None
    first_name: str | None
    last_name: str | None
    birthday: date | None
    picture_url: str | None
    banner_image_url: str | None
    preferred_gender: str | None
    about_me: str | None


class ProfileOut(BaseModel):
    nearest_airport: str | None
    can_fly_international: bool | None
    first_name: str | None
    last_name: str | None
    birthday: str | None
    picture_url: str | None
    banner_image_url: str | None
    preferred_gender: str | None
    about_me: str | None
