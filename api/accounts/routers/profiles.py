from fastapi import APIRouter, Depends
from common.authenticator import authenticator
from accounts.models import ProfileOut, ProfileIn
from accounts.queries.profiles import ProfileRepository


router = APIRouter()


# Not for Front-End
@router.post("/profiles", response_model=ProfileOut)
def create_profile(
    user: dict = Depends(authenticator.get_current_account_data),
    repo: ProfileRepository = Depends()
):
    return repo.create(user["id"])


@router.put("/profiles", response_model=ProfileOut)
def update_profile(
    profile: ProfileIn,
    user: dict = Depends(authenticator.get_current_account_data),
    repo: ProfileRepository = Depends()
):
    return repo.update(user["id"], profile)


@router.get("/profiles", response_model=ProfileOut)
def get_profile(
    user: dict = Depends(authenticator.get_current_account_data),
    repo: ProfileRepository = Depends()
):
    return repo.get(user["id"])
