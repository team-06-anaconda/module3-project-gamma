from common.client import Queries
from trips.models import TripIn, TripOut, TripList
from bson.objectid import ObjectId


class TripsRepository(Queries):
    DB_NAME = "touchFG"
    COLLECTION = "trips"

    def create_trip(self, trip: TripIn, user_id: str):
        trip = trip.dict()
        trip["arrival"] = str(trip["arrival"])
        trip["departure"] = str(trip["departure"])
        trip["user_id"] = user_id
        for attraction in trip["attraction"]["attractions"]:
            if attraction["bookmarked"] is None:
                attraction["bookmarked"] = False
        self.collection.insert_one(trip)
        trip["id"] = str(trip["_id"])
        return TripOut(**trip)

    def get_all_trips(self, user_id: str):
        trips = list(self.collection.find({"user_id": user_id}))
        for trip in trips:
            trip["id"] = str(trip["_id"])

        return TripList(**{"trips": trips})

    def get_trip_details(self, trip_id: str, user_id: str):
        trip = self.collection.find_one({"user_id": user_id, "_id": ObjectId(trip_id)})  # noqa
        trip["id"] = str(trip["_id"])
        return TripOut(**trip)

    def update_trip(self, trip_id: str, user_id: str, trip: TripIn):
        query = ({"_id": ObjectId(trip_id), "user_id": user_id})
        trip_input = trip.dict()
        trip_input["arrival"] = str(trip_input["arrival"])
        trip_input["departure"] = str(trip_input["departure"])
        for property in trip_input:
            if trip_input[property] is None:
                del trip_input[property]
        self.collection.update_one(query, {"$set": trip_input})
        updated_trip = self.collection.find_one(query)
        updated_trip["id"] = str(updated_trip["_id"])
        return TripOut(**updated_trip)

    def delete_trip(self, trip_id: str, user_id: str):
        self.collection.delete_one({"user_id": user_id, "_id": ObjectId(trip_id)})  # noqa
        return {"message": "trip has been deleted"}
