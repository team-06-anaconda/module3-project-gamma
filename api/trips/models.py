from pydantic import BaseModel
from datetime import date
from external_api.locations.models import Location
from external_api.attractions.models import AttractionOut


class TripIn(BaseModel):
    location: Location
    attraction: AttractionOut
    arrival: date
    departure: date


class TripOut(TripIn):
    id: str


class TripList(BaseModel):
    trips: list[TripOut]
