from fastapi import APIRouter, Depends
from trips.models import TripIn, TripOut, TripList
from trips.queries.trips import TripsRepository
from common.authenticator import authenticator

router = APIRouter()


@router.post("/trips", response_model=TripOut)
def create_trip(
    trip: TripIn,
    user: dict = Depends(authenticator.get_current_account_data),
    repo: TripsRepository = Depends()
):
    return repo.create_trip(trip, user["id"])


@router.get("/trips", response_model=TripList)
def get_all_trips(
    user: dict = Depends(authenticator.get_current_account_data),
    repo: TripsRepository = Depends()
):
    return repo.get_all_trips(user["id"])


@router.get("/trips/{trip_id}", response_model=TripOut)
def get_trip_details(
    trip_id: str,
    user: dict = Depends(authenticator.get_current_account_data),
    repo: TripsRepository = Depends()
):
    return repo.get_trip_details(trip_id, user["id"])


@router.put("/trips/{trip_id}", response_model=TripOut)
def update_trip(
    trip: TripIn,
    trip_id: str,
    user: dict = Depends(authenticator.get_current_account_data),
    repo: TripsRepository = Depends()
):
    return repo.update_trip(trip_id, user["id"], trip)


@router.delete("/trips/{trip_id}")
def delete_trip(
    trip_id: str,
    user: dict = Depends(authenticator.get_current_account_data),
    repo: TripsRepository = Depends()
):
    return repo.delete_trip(trip_id, user["id"])
