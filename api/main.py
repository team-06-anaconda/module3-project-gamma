from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from common.authenticator import authenticator
from accounts.routers import accounts
from external_api.weather.routers import weather
from external_api.attractions.routers import attractions
from external_api.locations.routers import locations
from external_api.pictures.routers import pexels
from accounts.routers import profiles
from trips.routers import trips
import os


app = FastAPI()
app.include_router(authenticator.router, tags=["Accounts"])
app.include_router(accounts.router, tags=["Accounts"])
app.include_router(weather.router, tags=["Weather"])
app.include_router(attractions.router, tags=["Attractions"])
app.include_router(pexels.router, tags=["Pictures"])
app.include_router(locations.router, tags=["Locations"])
app.include_router(profiles.router, tags=["Profiles"])
app.include_router(trips.router, tags=["Trips"])
app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "http://localhost:3000",
        "https://team-06-anaconda.gitlab.io/tripout",
        os.environ.get("CORS_HOST", None)
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def home():
    return True
