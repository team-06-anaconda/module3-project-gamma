from fastapi.testclient import TestClient
from main import app
from common.authenticator import authenticator
from external_api.attractions.queries.attractions import AttractionsQueries

client = TestClient(app)


class FakeAttractionsQueries:
    def get_coords(self, city: str, country: str):
        return [{"lat": 13829, "lon": 14289}]

    def get_attractions(self,
                        lat: str,
                        lon: str,
                        radius: int,
                        limit: int,
                        query: str
                        ):
        return [{
            "poi": {
                "name": "220 Second To None Auto Detail",
                "url": "www.220autodetail.biz/",
                "phone": "+1 866-959-6973"
            },
            "score": 0.8358505368,
            "address": {
                "freeformAddress": "South Vermont Avenue, LA, CA 90006"
            },
            "bookmarked": False
        }]


def fake_user_data():
    return {"id": "FAKEUSER", "username": "FAKER"}


def test_get_attraction_data():
    app.dependency_overrides[AttractionsQueries] = FakeAttractionsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_user_data

    res = client.get("/attractions",
                     params={"city": "Los Angeles", "country": "America"})

    data = res.json()

    assert res.status_code == 200
    assert data == {
        "attractions": [{
            "poi": {
                "name": "220 Second To None Auto Detail",
                "url": "www.220autodetail.biz/",
                "phone": "+1 866-959-6973"
            },
            "score": 0.8358505368,
            "address": {
                "freeformAddress": "South Vermont Avenue, LA, CA 90006"
            },
            "bookmarked": False
        }]
    }
