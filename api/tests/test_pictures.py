from fastapi.testclient import TestClient
from main import app
from common.authenticator import authenticator
from external_api.pictures.queries.pexels import PexelsQuery

client = TestClient(app)


class FakePexelsQuery:
    def get_picture(self, query: str):
        return (
            {
                "original": ("https://images.pexels.com/photos/2129796"
                             "/pexels-photo-2129796.png"),
                "small": ("https://images.pexels.com/photos/2129796"
                          "/pexels-photo-2129796.png?auto=compress&cs"
                          "=tinysrgb&h=130")
            }
        )


def fake_user_data():
    return {"id": "FAKEUSER", "username": "FAKER"}


def test_get_picture():
    app.dependency_overrides[PexelsQuery] = FakePexelsQuery
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_user_data

    res = client.get("/pictures", params={"query": "new%20york"})
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "original": ("https://images.pexels.com/photos/2129796"
                     "/pexels-photo-2129796.png"),
        "small": ("https://images.pexels.com/photos/2129796/pexels-photo-"
                  "2129796.png?auto=compress&cs=tinysrgb&h=130")
    }
