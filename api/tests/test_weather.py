from fastapi.testclient import TestClient
from main import app
from common.authenticator import authenticator
from external_api.weather.queries.weather import WeatherQueries

client = TestClient(app)


class FakeWeatherQuery:
    def get_location(self, city: str, country: str):
        return [{"lat": 100, "lon": 200}]

    def get_weather(self, lat: str, lon: str):
        return {
            "weather": [
                {
                    "main": "Clear",
                    "description": "clear sky"
                }
            ],
            "main": {
                "temp": 86.79,
                "feels_like": 85.53,
                "pressure": 1014,
                "humidity": 36,
                "temp_min": 82.58,
                "temp_max": 95.34
            },
            "wind": {
                "speed": 5.75,
                "deg": 280
            }
        }

    def get_location_key(self, lat: str, lon: str):
        return {"Key": "string"}

    def get_forecast(self, Key: str):
        return {
            "Headline": {
                "Text": "string",
                "Link": "string"
            },
            "DailyForecasts": [
                {
                    "Date": "2023-07-28T15:26:35+0000",
                    "Temperature": {
                        "Minimum": {
                            "Value": 0,
                            "Unit": "string"
                        },
                        "Maximum": {
                            "Value": 0,
                            "Unit": "string"
                        }
                    },
                    "Day": {
                        "Icon": 0,
                        "IconPhrase": "string"
                    },
                    "Night": {
                        "Icon": 0,
                        "IconPhrase": "string"
                    }
                }
            ]
        }


def fake_user_data():
    return {"id": "FAKEUSER", "username": "FAKER"}


def test_get_weather_data():
    app.dependency_overrides[WeatherQueries] = FakeWeatherQuery
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_user_data

    res = client.get(
        "/weather",
        params={"city": "Sacramento", "country": "America"}
    )
    data = res.json()

    assert res.status_code == 200
    assert data == {
                "weather": [
                    {
                        "main": "Clear",
                        "description": "clear sky"
                    }
                ],
                "main": {
                    "temp": 86.79,
                    "feels_like": 85.53,
                    "pressure": 1014,
                    "humidity": 36,
                    "temp_min": 82.58,
                    "temp_max": 95.34
                },
                "wind": {
                    "speed": 5.75,
                    "deg": 280
                }
            }


def test_get_forecast_data():
    app.dependency_overrides[WeatherQueries] = FakeWeatherQuery
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_user_data

    res = client.get(
        "/forecast",
        params={"country": "America", "city": "Sacramento"}
    )
    data = res.json()

    assert res.status_code == 200
    assert data == {
                "Headline": {
                    "Text": "string",
                    "Link": "string"
                },
                "DailyForecasts": [
                    {
                        "Date": "2023-07-28T15:26:35+00:00",
                        "Temperature": {
                            "Minimum": {
                                "Value": 0,
                                "Unit": "string"
                            },
                            "Maximum": {
                                "Value": 0,
                                "Unit": "string"
                            }
                        },
                        "Day": {
                            "Icon": 0,
                            "IconPhrase": "string"
                        },
                        "Night": {
                            "Icon": 0,
                            "IconPhrase": "string"
                        }
                    }
                ]
            }
