from fastapi.testclient import TestClient
from main import app
from trips.queries.trips import TripsRepository
from trips.models import TripIn
from common.authenticator import authenticator

client = TestClient(app)


class FakeTripsRepo:
    def create_trip(self, trip: TripIn, user_id: str):
        trip = trip.dict()
        return {
            "location": trip["location"],
            "attraction": trip["attraction"],
            "arrival": trip["arrival"],
            "departure": trip["departure"],
            "id": user_id
        }

    def get_all_trips(self, user_id: str):
        return {
            "trips": [
                {
                    "location": {
                        "country": {
                            "currency": {
                                "code": "c_code",
                                "name": "c_name"
                            },
                            "iso2": "iso2",
                            "capital": "capital",
                            "tourists": 100,
                            "name": "country_name",
                            "region": "country_region"
                        },
                        "city": {
                            "name": "city_name",
                            "latitude": "111",
                            "longitude": "222",
                            "population": 200
                        },
                        "airport": {
                            "name": "airport_name",
                            "city": "airport_city",
                            "region": "airport_region",
                            "timezone": "airport_timezone"
                        },
                        "exchange_rate": "exchange_rate",
                        "picture_url": {
                            "original": "picture_orig",
                            "small": "picture_small"
                        }
                    },
                    "attraction": {
                        "attractions": [
                            {
                                "poi": {
                                    "name": "poi_name",
                                    "url": "poi_url",
                                    "phone": "poi_phone"
                                },
                                "score": 98,
                                "address": {
                                    "freeformAddress": "poi_freeformAddress"
                                },
                                "bookmarked": False
                            }
                        ]
                    },
                    "arrival": "2023-11-24",
                    "departure": "2023-12-02",
                    "id": user_id
                }
            ]
        }

    def get_trip_details(self, trip_id: str, user_id: str):
        return {
            "location": {
                "country": {
                    "currency": {
                        "code": "string",
                        "name": "string"
                    },
                    "iso2": "string",
                    "capital": "string",
                    "tourists": 0,
                    "name": "string",
                    "region": "string"
                },
                "city": {
                    "name": "string",
                    "latitude": "string",
                    "longitude": "string",
                    "population": 0
                },
                "airport": {
                    "name": "string",
                    "city": "string",
                    "region": "string",
                    "timezone": "string"
                },
                "exchange_rate": "string",
                "picture_url": {
                    "original": "string",
                    "small": "string"
                }
            },
            "attraction": {
                "attractions": [
                    {
                        "poi": {
                            "name": "string",
                            "url": "string",
                            "phone": "string"
                        },
                        "score": 0,
                        "address": {
                            "freeformAddress": "string"
                        },
                        "bookmarked": False
                    }
                ]
            },
            "arrival": "2023-07-14",
            "departure": "2023-07-14",
            "id": user_id
        }

    def update_trip(self, trip_id: str, user_id: str, trip: TripIn):
        return {
            "location": {
                "country": {
                    "currency": {
                        "code": "string",
                        "name": "string"
                    },
                    "iso2": "string",
                    "capital": "string",
                    "tourists": 0,
                    "name": "string",
                    "region": "string"
                },
                "city": {
                    "name": "string",
                    "latitude": "string",
                    "longitude": "string",
                    "population": 0
                },
                "airport": {
                    "name": "string",
                    "city": "string",
                    "region": "string",
                    "timezone": "string"
                },
                "exchange_rate": "string",
                "picture_url": {
                    "original": "string",
                    "small": "string"
                }
            },
            "attraction": {
                "attractions": [
                    {
                        "poi": {
                            "name": "string",
                            "url": "string",
                            "phone": "string"
                        },
                        "score": 0,
                        "address": {
                            "freeformAddress": "string"
                        },
                        "bookmarked": False
                    }
                ]
            },
            "arrival": "2023-07-14",
            "departure": "2023-07-14",
            "id": user_id
        }


def fake_user_data():
    return {"id": "FAKEUSER", "username": "FAKER"}


def test_create_trip():
    app.dependency_overrides[TripsRepository] = FakeTripsRepo
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_user_data

    c_code = "JPY"
    c_name = "Yen"
    iso2 = "JP"
    capital = "tokyo"
    tourists = 200
    country_name = "Japan"
    country_region = "Kanto"

    city_name = "Tokyo"
    city_latitude = "111"
    city_longitude = "222"
    city_population = 12

    airport_name = "Mada mada"
    airport_city = "tokyo"
    airport_region = "kanto"
    airport_timezone = "JPTZ"

    exchange_rate = "133.38 Yen"
    picture_orig = "www.somepicture.img"
    picture_small = "www.somepicture.img/small"

    poi_name = "Restaurant"
    poi_url = "www.restaurant.com"
    poi_phone = "510101010"
    score = 98
    poi_freeformAddress = "123 some street"
    arrival = "2023-12-24"
    departure = "2023-12-29"

    body = {
            "location": {
                "country": {
                    "currency": {
                        "code": c_code,
                        "name": c_name
                    },
                    "iso2": iso2,
                    "capital": capital,
                    "tourists": tourists,
                    "name": country_name,
                    "region": country_region
                },
                "city": {
                    "name": city_name,
                    "latitude": city_latitude,
                    "longitude": city_longitude,
                    "population": city_population
                },
                "airport": {
                    "name": airport_name,
                    "city": airport_city,
                    "region": airport_region,
                    "timezone": airport_timezone
                },
                "exchange_rate": exchange_rate,
                "picture_url": {
                    "original": picture_orig,
                    "small": picture_small
                }
            },
            "attraction": {
                "attractions": [
                    {
                        "poi": {
                            "name": poi_name,
                            "url": poi_url,
                            "phone": poi_phone
                        },
                        "score": score,
                        "address": {
                            "freeformAddress": poi_freeformAddress
                        },
                        "bookmarked": False
                    }
                ]
            },
            "arrival": arrival,
            "departure": departure,
        }

    res = client.post("/trips", json=body)
    data = res.json()

    app.dependency_overrides = {}

    assert res.status_code == 200
    assert data == {
            "location": {
                "country": {
                    "currency": {
                        "code": c_code,
                        "name": c_name
                    },
                    "iso2": iso2,
                    "capital": capital,
                    "tourists": tourists,
                    "name": country_name,
                    "region": country_region
                },
                "city": {
                    "name": city_name,
                    "latitude": city_latitude,
                    "longitude": city_longitude,
                    "population": city_population
                },
                "airport": {
                    "name": airport_name,
                    "city": airport_city,
                    "region": airport_region,
                    "timezone": airport_timezone
                },
                "exchange_rate": exchange_rate,
                "picture_url": {
                    "original": picture_orig,
                    "small": picture_small
                }
            },
            "attraction": {
                "attractions": [
                    {
                        "poi": {
                            "name": poi_name,
                            "url": poi_url,
                            "phone": poi_phone
                        },
                        "score": score,
                        "address": {
                            "freeformAddress": poi_freeformAddress
                        },
                        "bookmarked": False
                    }
                ]
            },
            "arrival": arrival,
            "departure": departure,
            "id": "FAKEUSER"
        }


def test_get_all_trips():
    app.dependency_overrides[TripsRepository] = FakeTripsRepo
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_user_data

    res = client.get("/trips")
    data = res.json()

    assert res.status_code == 200
    assert data == {
            "trips": [
                {
                    "location": {
                        "country": {
                            "currency": {
                                "code": "c_code",
                                "name": "c_name"
                            },
                            "iso2": "iso2",
                            "capital": "capital",
                            "tourists": 100,
                            "name": "country_name",
                            "region": "country_region"
                        },
                        "city": {
                            "name": "city_name",
                            "latitude": "111",
                            "longitude": "222",
                            "population": 200
                        },
                        "airport": {
                            "name": "airport_name",
                            "city": "airport_city",
                            "region": "airport_region",
                            "timezone": "airport_timezone"
                        },
                        "exchange_rate": "exchange_rate",
                        "picture_url": {
                            "original": "picture_orig",
                            "small": "picture_small"
                        }
                    },
                    "attraction": {
                        "attractions": [
                            {
                                "poi": {
                                    "name": "poi_name",
                                    "url": "poi_url",
                                    "phone": "poi_phone"
                                },
                                "score": 98,
                                "address": {
                                    "freeformAddress": "poi_freeformAddress"
                                },
                                "bookmarked": False
                            }
                        ]
                    },
                    "arrival": "2023-11-24",
                    "departure": "2023-12-02",
                    "id": "FAKEUSER"
                }
            ]
        }


def test_get_trip_details():
    app.dependency_overrides[TripsRepository] = FakeTripsRepo
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_user_data

    trip_id = "string"
    res = client.get(f"/trips/{trip_id}")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "location": {
            "country": {
                "currency": {
                    "code": "string",
                    "name": "string"
                },
                "iso2": "string",
                "capital": "string",
                "tourists": 0,
                "name": "string",
                "region": "string"
            },
            "city": {
                "name": "string",
                "latitude": "string",
                "longitude": "string",
                "population": 0
            },
            "airport": {
                "name": "string",
                "city": "string",
                "region": "string",
                "timezone": "string"
            },
            "exchange_rate": "string",
            "picture_url": {
                "original": "string",
                "small": "string"
            }
        },
        "attraction": {
            "attractions": [
                {
                    "poi": {
                        "name": "string",
                        "url": "string",
                        "phone": "string"
                    },
                    "score": 0,
                    "address": {
                        "freeformAddress": "string"
                    },
                    "bookmarked": False
                }
            ]
        },
        "arrival": "2023-07-14",
        "departure": "2023-07-14",
        "id": "FAKEUSER"
    }


def test_update_trip():
    app.dependency_overrides[TripsRepository] = FakeTripsRepo
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_user_data

    body = {
        "location": {
            "country": {
                "currency": {
                    "code": "string",
                    "name": "string"
                },
                "iso2": "string",
                "capital": "string",
                "tourists": 0,
                "name": "string",
                "region": "string"
            },
            "city": {
                "name": "string",
                "latitude": "string",
                "longitude": "string",
                "population": 0
            },
            "airport": {
                "name": "string",
                "city": "string",
                "region": "string",
                "timezone": "string"
            },
            "exchange_rate": "string",
            "picture_url": {
                "original": "string",
                "small": "string"
            }
        },
        "attraction": {
            "attractions": [
                {
                    "poi": {
                        "name": "string",
                        "url": "string",
                        "phone": "string"
                    },
                    "score": 0,
                    "address": {
                        "freeformAddress": "string"
                    },
                    "bookmarked": False
                }
            ]
        },
        "arrival": "2023-07-14",
        "departure": "2023-07-14"
    }

    trip_id = "string"
    res = client.put(f"/trips/{trip_id}", json=body)
    assert res.status_code == 200
    data = res.json()

    assert data == {
        "location": {
            "country": {
                "currency": {
                    "code": "string",
                    "name": "string"
                },
                "iso2": "string",
                "capital": "string",
                "tourists": 0,
                "name": "string",
                "region": "string"
            },
            "city": {
                "name": "string",
                "latitude": "string",
                "longitude": "string",
                "population": 0
            },
            "airport": {
                "name": "string",
                "city": "string",
                "region": "string",
                "timezone": "string"
            },
            "exchange_rate": "string",
            "picture_url": {
                "original": "string",
                "small": "string"
            }
        },
        "attraction": {
            "attractions": [
                {
                    "poi": {
                        "name": "string",
                        "url": "string",
                        "phone": "string"
                    },
                    "score": 0,
                    "address": {
                        "freeformAddress": "string"
                    },
                    "bookmarked": False
                }
            ]
        },
        "arrival": "2023-07-14",
        "departure": "2023-07-14",
        "id": "FAKEUSER"
    }
