from fastapi.testclient import TestClient
from main import app
from common.authenticator import authenticator
from external_api.locations.queries.locations import ApiQueryRepository

client = TestClient(app)


class FakeApiQueryRepository:
    def get_country(self, country: str):
        return ({
            "currency": {
                "code": "USD",
                "name": "Us Dollar"
            },
            "iso2": "US",
            "capital": "Washington, D.C.",
            "tourists": 79746,
            "name": country,
            "region": "Northern America",
        })

    def get_city(self, city: str):
        return (
            {
                "name": city,
                "latitude": "32.7936",
                "longitude": "-96.7662",
                "population": 5743938
            }
        )

    def get_airport(self, city: str, country_code: str):
        return (
            {
                "name": "test-airports",
                "city": "Denver",
                "region": "North Carolina",
                "timezone": "America/New_York"
            }
        )

    def get_exchange_rate(self, currency_code: str):
        return (
            {
                "new_currency": "JPY",
                "new_amount": 138.82
            }
        )

    def get_location():
        return (
            {
                "country": {
                    "currency": {
                        "code": "EUR",
                        "name": "Euro"
                    },
                    "iso2": "ES",
                    "capital": "Madrid",
                    "tourists": 82773,
                    "name": "Spain",
                    "region": "Southern Europe"
                },
                "city": {
                    "name": "Madrid",
                    "latitude": "40.4189",
                    "longitude": "-3.6919",
                    "population": 3266126
                },
                "airport": {
                    "name": "Cuatro Vientos Airport",
                    "city": "Madrid",
                    "region": "Madrid",
                    "timezone": "Europe/Madrid"
                },
                "exchange_rate": "1$ = 0.89 EUR",
                "picture_url": {
                    "original": ("https://images.pexels.com"
                                 "/photos/670261/pexels-photo"
                                 "-670261.jpeg"),
                    "small": ("https://images.pexels.com"
                              "/photos/670261/pexels-photo-670261"
                              ".jpeg?auto=compress&cs=tinysrgb&h=130")
                }
            }
        )


def fake_user_data():
    return {"id": "FAKEUSER", "username": "FAKER"}


def test_get_country():
    app.dependency_overrides[ApiQueryRepository] = FakeApiQueryRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_user_data

    res = client.get("/countries?country=test-countries")
    data = res.json()
    assert res.status_code == 200
    assert data == {
                    "currency": {
                        "code": "USD",
                        "name": "Us Dollar"
                    },
                    "iso2": "US",
                    "capital": "Washington, D.C.",
                    "tourists": 79746,
                    "name": "test-countries",
                    "region": "Northern America"
                }


def test_get_city():
    app.dependency_overrides[ApiQueryRepository] = FakeApiQueryRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_user_data

    res = client.get("/cities?city=test-cities")
    data = res.json()
    assert res.status_code == 200
    assert data == {
            "name": "test-cities",
            "latitude": "32.7936",
            "longitude": "-96.7662",
            "population": 5743938
        }


def test_get_airport():
    app.dependency_overrides[ApiQueryRepository] = FakeApiQueryRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_user_data

    res = client.get("/airports?city=test-airports&"
                     "country_code=test-countries")
    data = res.json()
    assert res.status_code == 200
    assert data == {
        "name": "test-airports",
        "city": "Denver",
        "region": "North Carolina",
        "timezone": "America/New_York"
    }


def test_get_exchange_rate():
    app.dependency_overrides[ApiQueryRepository] = FakeApiQueryRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_user_data

    res = client.get("/exchange_rates?currency_code=JPY")
    data = res.json()
    assert res.status_code == 200
    assert data == {
        "new_currency": "JPY",
        "new_amount": 138.82,
    }
