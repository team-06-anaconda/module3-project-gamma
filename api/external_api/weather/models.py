from pydantic import BaseModel
from datetime import datetime


class Location(BaseModel):
    city: str
    country: str


class WeatherCoord(BaseModel):
    lat: str
    lon: str


class Weather(BaseModel):
    main: str
    description: str


class WeatherMain(BaseModel):
    temp: float
    feels_like: float
    pressure: float
    humidity: float
    temp_min: float
    temp_max: float


class WeatherWind(BaseModel):
    speed: float
    deg: float


class WeatherOut(BaseModel):
    weather: list[Weather]
    main: WeatherMain
    wind: WeatherWind


class LocationKeyOut(BaseModel):
    Key: str


class MinimumOut(BaseModel):
    Value: int
    Unit: str


class MaximumOut(BaseModel):
    Value: int
    Unit: str


class TempOut(BaseModel):
    Minimum: MinimumOut
    Maximum: MaximumOut


class DayOut(BaseModel):
    Icon: int
    IconPhrase: str


class NightOut(BaseModel):
    Icon: int
    IconPhrase: str


class DailyForecasts(BaseModel):
    Date: datetime
    Temperature: TempOut
    Day: DayOut
    Night: NightOut


class HeadlineOut(BaseModel):
    Text: str
    Link: str


class ForecastOut(BaseModel):
    Headline: HeadlineOut
    DailyForecasts: list[DailyForecasts]
