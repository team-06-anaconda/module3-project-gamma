from fastapi import APIRouter, Depends, status, HTTPException
from common.authenticator import authenticator
from external_api.weather.models import WeatherCoord, WeatherOut
from external_api.weather.queries.weather import WeatherQueries, LimitExceeded
from external_api.weather.models import LocationKeyOut, ForecastOut


router = APIRouter()


@router.get("/location", response_model=WeatherCoord)
# Not for front end
async def get_location(
    city: str,
    country: str,
    queries: WeatherQueries = Depends(),
    user: dict = Depends(authenticator.get_current_account_data)
):
    data = queries.get_location(city, country)

    if data:
        lat = data[0].get("lat")
        lon = data[0].get('lon')

        if lat is not None and lon is not None:
            return WeatherCoord(lat=lat, lon=lon)

    return WeatherCoord(lat="", lon="")


@router.get("/weather", response_model=WeatherOut)
async def get_weather_data(
    city: str,
    country: str,
    queries: WeatherQueries = Depends(),
    user: dict = Depends(authenticator.get_current_account_data)
):
    coords = queries.get_location(city, country)[0]
    weather_data = queries.get_weather(coords.get("lat"), coords.get("lon"))
    return WeatherOut(**weather_data)


@router.get("/location_key", response_model=LocationKeyOut)
async def get_location_key(
    city: str,
    country: str,
    queries: WeatherQueries = Depends(),
    user: dict = Depends(authenticator.get_current_account_data)
):
    coords = queries.get_location(city, country)[0]
    lat = coords.get("lat")
    lon = coords.get("lon")
    try:
        location_key = queries.get_location_key(lat, lon)["Key"]
    except LimitExceeded:
        raise HTTPException(
            status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
            detail="""API fetch limit exceeded. Only 50 are allowed per day.
                We are poor and have no funding :("""
        )
    return LocationKeyOut(**location_key)


@router.get("/forecast", response_model=ForecastOut)
async def get_forecast(
    city: str,
    country: str,
    queries: WeatherQueries = Depends(),
    user: dict = Depends(authenticator.get_current_account_data)
):
    coords = queries.get_location(city, country)[0]
    lat = coords.get("lat")
    lon = coords.get("lon")
    try:
        location_key = queries.get_location_key(lat, lon)["Key"]
        forecast = queries.get_forecast(location_key)
    except LimitExceeded:
        raise HTTPException(
            status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
            detail="""API fetch limit exceeded. Only 50 are allowed per day.
                We are poor and have no funding :("""
        )
    return ForecastOut(**forecast)
