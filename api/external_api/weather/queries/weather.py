import requests
import os
import json


class LimitExceeded(Exception):
    pass


class WeatherQueries:
    def get_location(self, city: str, country: str):
        key = os.environ['OPEN_WEATHER_API_KEY']
        url = f"http://api.openweathermap.org/geo/1.0/\
direct?q={city},{country}&appid={key}"
        res = requests.get(url)
        data = json.loads(res.text)
        return data

    def get_weather(self, lat: str, lon: str):
        key = os.environ['OPEN_WEATHER_API_KEY']
        url = f"https://api.openweathermap.org/data/2.5/weather\
?lat={lat}&lon={lon}&appid={key}&units=imperial"
        res = requests.get(url)
        data = json.loads(res.text)
        return data

    def get_location_key(self, lat: str, lon: str):
        key = os.environ['FORECAST_KEY']
        url = f"http://dataservice.accuweather.com/locations/v1/cities/\
geoposition/search?apikey={key}&q={lat},{lon}"
        res = requests.get(url)
        if res.status_code == 503:
            raise LimitExceeded
        data = json.loads(res.text)
        return data

    def get_forecast(self, locationKey: str):
        key = os.environ['FORECAST_KEY']
        url = f"http://dataservice.accuweather.com/forecasts/v1/daily/5day/\
{locationKey}?apikey={key}"
        res = requests.get(url)
        if res.status_code == 503:
            raise LimitExceeded
        data = json.loads(res.text)
        return data
