from fastapi import APIRouter, Depends
from common.authenticator import authenticator
from external_api.pictures.queries.pexels import PexelsQuery
from external_api.pictures.models import Picture


router = APIRouter()


@router.get("/pictures", response_model=Picture)
def get_picture(
    query: str,
    repo: PexelsQuery = Depends(),
    user: dict = Depends(authenticator.get_current_account_data)
):
    return repo.get_picture(query)
