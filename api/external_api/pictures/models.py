from pydantic import BaseModel


class Picture(BaseModel):
    original: str
    small: str
