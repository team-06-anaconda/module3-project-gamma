import requests
import json
import os
from external_api.pictures.models import Picture


class PexelsQuery:

    def get_picture(self, query: str):
        url = "https://api.pexels.com/v1/search"
        params = {"query": query, "per_page": 1}
        headers = {"Authorization": os.environ["PEXELS_KEY"]}

        response = requests.get(url, params=params, headers=headers)
        data = json.loads(response.text)

        return Picture(**data["photos"][0]["src"])
