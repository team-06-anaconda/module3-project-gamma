from fastapi import APIRouter, Depends, status, HTTPException
from common.authenticator import authenticator
from external_api.attractions.queries.attractions import (
    AttractionsQueries,
    NoResults
)
from external_api.attractions.models import AttractionOut
from external_api.weather.models import WeatherCoord


coordRouter = APIRouter()
router = APIRouter()


@coordRouter.get("/coordinates", response_model=WeatherCoord)
def get_coordinates(
    city: str,
    country: str,
    queries: AttractionsQueries = Depends(),
    user: dict = Depends(authenticator.get_current_account_data)
):
    coords = queries.get_coords(city, country)
    return coords


@router.get("/attractions", response_model=AttractionOut)
def get_attraction_data(
    city: str,
    country: str,
    radius: int | None = 32000,
    limit: int | None = 51,
    query: str | None = "tours",
    queries: AttractionsQueries = Depends(),
    user: dict = Depends(authenticator.get_current_account_data)
):
    coords = queries.get_coords(city, country)[0]
    try:
        attractions = queries.get_attractions(coords.get("lat"), coords.get("lon"), radius, limit, query)  # noqa
    except NoResults:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="No attractions found to match that query"
        )

    if attractions:
        return AttractionOut(attractions=attractions)
