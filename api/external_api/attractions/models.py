from pydantic import BaseModel


class Attraction(BaseModel):
    lat: str
    lon: str


class POI(BaseModel):
    name: str
    url: str | None
    phone: str | None


class Address(BaseModel):
    freeformAddress: str


class Results(BaseModel):
    poi: POI
    score: float
    address: Address
    bookmarked: bool | None


class AttractionIn(BaseModel):
    lat: str
    lon: str
    radius: int
    limit: int
    query: str


class AttractionOut(BaseModel):
    attractions: list[Results]
