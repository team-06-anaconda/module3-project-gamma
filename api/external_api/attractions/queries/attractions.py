import requests
import os
import json


class NoResults(BaseException):
    pass


class AttractionsQueries:
    def get_coords(self, city: str, country: str):
        key = os.environ['OPEN_WEATHER_API_KEY']
        url = ("http://api.openweathermap.org/geo/1.0/direct"
               f"?q={city},{country}&appid={key}")
        res = requests.get(url)
        coordinate_data = json.loads(res.text)
        return coordinate_data

    def get_attractions(self, lat: str, lon: str,
                        radius: int, limit: int, query: str):
        key = os.environ['ATTRACTION_API_KEY']
        url = f"https://api.tomtom.com/search/2/poiSearch/\
{query}.json?key={key}&typeahead=true&limit={limit}\
&lat={lat}&lon={lon}&radius={radius}"
        params = {"lat": lat, "lon": lon}
        res = requests.get(url, params=params)
        data = json.loads(res.text)
        dR = data["results"]
        attr = [att for att in dR if att["poi"]["name"] != "None"]

        if len(attr) == 0:
            raise NoResults

        return attr
