from pydantic import BaseModel
from external_api.pictures.queries.pexels import Picture


class LocationIn(BaseModel):
    country: str
    city: str
    population: int
    continent_region: str
    currency_name: str
    capital: str
    tourists: int
    timezone: str
    currency_exchange_rate: str
    climate: str
    nearest_airport: str
    airport_city: str
    country_region: str


class Currency(BaseModel):
    code: str
    name: str


class Country(BaseModel):
    currency: Currency
    iso2: str
    capital: str
    tourists: int
    name: str
    region: str


class City(BaseModel):
    name: str
    latitude: str
    longitude: str
    population: int


class Airport(BaseModel):
    name: str
    city: str
    region: str
    timezone: str


class CurrencyExc(BaseModel):
    new_currency: str
    new_amount: float


class Location(BaseModel):
    country: Country
    city: City
    airport: Airport | None
    exchange_rate: str
    picture_url: Picture
