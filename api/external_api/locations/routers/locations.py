from fastapi import APIRouter, Depends, status, HTTPException
from common.authenticator import authenticator
from external_api.pictures.queries.pexels import PexelsQuery
from external_api.locations.queries.locations import (
    ApiQueryRepository, NotFound
)
from external_api.locations.models import (
    Country, City, Airport,
    CurrencyExc, Location
)


router = APIRouter()


# Not for Front-End
@router.get("/countries", response_model=Country)
def get_country_data(
    country: str,
    repo: ApiQueryRepository = Depends(),
    user: dict = Depends(authenticator.get_current_account_data)
):
    try:
        country = repo.get_country(country)
    except NotFound:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Cannot find country data",
        )

    return country


# Not for Front-End
@router.get("/cities", response_model=City)
def get_city_data(
    city: str,
    repo: ApiQueryRepository = Depends(),
    user: dict = Depends(authenticator.get_current_account_data)
):
    try:
        city = repo.get_city(city)
    except NotFound:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Cannot find city data",
        )

    return city


# Not for Front-End
@router.get("/airports", response_model=Airport | None)
def get_airport_data(
    city: str,
    country_code: str,
    repo: ApiQueryRepository = Depends(),
    user: dict = Depends(authenticator.get_current_account_data)
):
    return repo.get_airport(city, country_code)


# Not for Front-End
@router.get("/exchange_rates", response_model=CurrencyExc)
def get_exchange_rate(
    currency_code: str,
    repo: ApiQueryRepository = Depends(),
    user: dict = Depends(authenticator.get_current_account_data)
):
    return repo.get_exchange_rate(currency_code)


@router.get("/locations", response_model=Location)
def get_location_data(
    country: str,
    city: str,
    repo: ApiQueryRepository = Depends(),
    picture: PexelsQuery = Depends()
):
    try:
        country = repo.get_country(country)
        city = repo.get_city(city)

    except NotFound:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Can't find the related city and country pair"
        )

    airport = repo.get_airport(city.name, country.iso2)
    exchange = repo.get_exchange_rate(country.currency.code)
    photo_url = picture.get_picture(city.name)
    location = {
        "country": country,
        "city": city,
        "picture_url": photo_url,
        "airport": airport,
        "exchange_rate": f"1$ = {exchange.new_amount} {exchange.new_currency}"

    }

    return location
