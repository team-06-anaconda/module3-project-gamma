import requests
import os
import json
from external_api.locations.models import (
    Country,
    City,
    Airport,
    CurrencyExc,
)


class NotFound(Exception):
    pass


class ApiQueryRepository:
    @property
    def auth_key(self):
        return {"x-Api-Key": os.environ['NINJA_KEY']}

    def get_country(self, country: str):
        url = f'https://api.api-ninjas.com/v1/country?name={country}'
        response = requests.get(url, headers=self.auth_key)
        data = json.loads(response.text)

        if len(data) == 0:
            raise NotFound

        return Country(**data[0])

    def get_city(self, city: str):
        url = f"https://api.api-ninjas.com/v1/city?name={city}"
        response = requests.get(url, headers=self.auth_key)
        data = json.loads(response.text)

        if len(data) == 0:
            raise NotFound

        return City(**data[0])

    def get_airport(self, city: str, country_code: str):
        url = "https://api.api-ninjas.com/v1/airports"
        params = {"city": city, "country": country_code}
        response = requests.get(url, params=params, headers=self.auth_key)
        data = json.loads(response.text)

        if len(data) == 0:
            return None

        return Airport(**data[0])

    def get_exchange_rate(self, new_currency: str):
        url = f"https://api.api-ninjas.com/v1/convertcurrency?\
have=USD&want={new_currency}&amount=1"
        response = requests.get(url, headers=self.auth_key)

        data = json.loads(response.text)
        return CurrencyExc(**data)
