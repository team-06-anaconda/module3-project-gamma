## Integrations

The application will need to get the following kinds of data from third-party sources:

- Location from API-Ninjas City, Country, Airports, and Exchange-Rate APIs
- Weather from OpenWeatherMap Current Weather Data API
- Pictures from Pexels API
- Attractions from TomTom Developer Points of Interest API
- Forecasts from AccuWeather 5-day Forecast API. Unfortunatly there are a limited number of calls per day: 50
