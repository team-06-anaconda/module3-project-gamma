# APIs

## Weather

- **Method**:  `GET`
- **Path**: `/location`

Input:

```json
{
   "city": "string",
   "country": "string"
}
```

Output:

```json
{
  "lat": "string",
  "lon": "string"
}
```



- **Method**:  `GET`
- **Path**: `/weather`

Input:

```json
{
   "city": "string",
   "country": "string"
}
```

Output:

```json
{
  "weather": [
    {
      "main": "string"
    }
  ],
  "main": {
    "temp": "int",
    "feels_like": "int",
    "pressure": "int",
    "humidity": "int",
    "temp_min": "int",
    "temp_max": "int"
  },
  "wind": {
    "speed": "int",
    "deg": "int"
  }
}
```
Weather information is obtained when searching a city and country and displayed in results page.

## Attractions

- **Method**: `GET`
- **Path**: `/attractions`

Input:

```json
{
    "city": "string",
   "country": "string"
}
```

Output:

```json
{
  "attractions": [
    {
      "poi": {
        "name": "string",
        "url": "string",
        "phone": "string"
      },
      "score": "int",
      "address": {
        "freeformAddress": "string"
      },
      "bookmarked": "bool"
    }
  ]
}
```
Attractions nearby is obtained when searching a city and country and is displayed on the results page.


## Pictures

- **Method**: `GET`
- **Path**: `/pictures`

Input:

```json
{
    "query": "string"
}
```

Output:

```json
{
  "original": "string",
  "small": "string"
}
```

Pictures are rendered on the main page and when searching city and country on the results page.

## Locations

- **Method**: `GET`
- **Path**: `/locations`

Input:

```json
{
    "city": "string",
   "country": "string"
}
```

Output:

```json
{
  "country": {
    "currency": {
      "code": "string",
      "name": "string"
    },
    "iso2": "string",
    "capital": "string",
    "tourists": "int",
    "name": "string",
    "region": "string"
  },
  "city": {
    "name": "string",
    "latitude": "string",
    "longitude": "string",
    "population": "int"
  },
  "airport": {
    "name": "string",
    "city": "string",
    "region": "string",
    "timezone": "string"
  },
  "exchange_rate": "string",
  "picture_url": {
    "original": "string",
    "small": "string"
  }
}
```

Locations endpoint is where a majority of apis are called in one. This is the bulk of the results page.


## Trips

- **Method**: `GET`,`POST`, `GET`, `PUT`, `DELETE`,
- **Path**: `/trips`, `/trips/{trip_id}`

Input:

```json
{
   "trip_id": "string"
}
```

Output:

```json
{
  "location": {
    "country": {
      "currency": {
        "code": "string",
        "name": "string"
      },
      "iso2": "string",
      "capital": "string",
      "tourists": "int",
      "name": "string",
      "region": "string"
    },
    "city": {
      "name": "string",
      "latitude": "string",
      "longitude": "string",
      "population": "int"
    },
    "airport": {
      "name": "string",
      "city": "string",
      "region": "string",
      "timezone": "string"
    },
    "exchange_rate": "string",
    "picture_url": {
      "original": "string",
      "small": "string"
    }
  },
  "attraction": {
    "attractions": [
      {
        "poi": {
          "name": "string",
          "url": "string",
          "phone": "string"
        },
        "score": "int",
        "address": {
          "freeformAddress": "string"
        },
        "bookmarked": "bool"
      }
    ]
  },
  "arrival": "stringdate",
  "departure": "stringdate",
  "id": "string"
}
```
Users can save trips to their 'My Trips' page for easy access. They can edit dates and save attractions to their itinerary.

## Accounts

- **Method**: `GET`, `POST`, `DELETE`, `POST`
- **Path**: `/accounts`

Input:

```json
{
  "password": "string",
  "password2": "string",
  "email": "string"
}
```

Output:

```json
{
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
    "id": "string",
    "email": "string"
  }
}
```

The Accounts API will create/delete a user account. This also houses the token authentication.


## Profiles

- **Method**: `GET`, `PUT`, `POST`
- **Path**: `/profiles`

Input:

```json
{
    "fastapi_token": "string"
}
```

Output:

```json
{
  "nearest_airport": "string",
  "can_fly_international": "bool",
  "first_name": "string",
  "last_name": "string",
  "birthday": "stringdate",
  "picture_url": "string",
  "banner_image_url": "string",
  "preferred_gender": "string",
  "about_me": "string"
}
```

Once a user has created an account, a profile is created on their behalf. They can then go and update it with information that suits them.

## Forecasts


- **Method**: `GET`
- **Path**: `/location_key`

Input:

```json
{
    "city":"string",
    "country":"string"
}
```

Output:

```json
{
  "Key": "string"
}
```

- **Method**: `GET`
- **Path**: `/forecast`

Input:

```json
{
    "city":"string",
    "country":"string"
}
```

Output:

```json
{
  "Headline": {
    "Text": "string"
  },
  "DailyForecasts": [
    {
      "Date": "stringdate",
      "Temperature": {
        "Minimum": {
          "Value": 0,
          "Unit": "string"
        },
        "Maximum": {
          "Value": 0,
          "Unit": "string"
        }
      },
      "Day": {
        "Icon": 0,
        "IconPhrase": "string"
      },
      "Night": {
        "Icon": 0,
        "IconPhrase": "string"
      },
      "Link": "string"
    }
  ]
}
```
Forecast data for next five days. This API only allows 50 calls per day. We have set up error handling to take care of this inconvenience.
