# Customer Graphical Human Interface

## Home Page

The Landing page of the website, it has two views, one for logged in users and one for anonymous users. For logged in users, the location cards down below become clickable and can take them to the search result page with the corresponding location and attraction information.

![Home Page](wireframes/Home_page.png)

## Login Page

Simple Basic Login Page, When bad credentials are submitted will display a simple grey p-tag message below the submit button notifying the user. Will redirect to Home Page after a successful login.

![Alt text](wireframes/Login_page.png)

## SignUp Page

SignUp page requiring a unique email as a username field and a password and confirm password input fields. Will display a simple error message below the submit button to notify user of bad input in the case of mismatched passwords or registering with an email that already has an account.

![Alt text](wireframes/Signup_page.png)

## Trip Search Result Page

The Main component of the website, this page display the results of searching for a city and country. The data on the page includes basic information about the city and country, the nearest airport to the location, exchange rates and current weather.

On the right bar, an attractions list will be displayed for available points of interest, and the logged in user can search for different points of interest using the search bar above.

![Alt text](wireframes/Trip_search_result_page.png)

Below in the weather card, there's the current weather data as well as a button that would then pop a modal open and show you the weather forecast for the next 5 days.

![Alt text](wireframes/trip_search_results_weather_modal.png)

Upon hitting the "Add to my trips" button, a modal will pop up allowing the user to save their planned arrival and departure times. Upon submit, they will be redirected to the My trips page to see that their trip has been bookmarked.

![Alt text](wireframes/trip_search_results_modal.png)

## My Trips Page

On this page, the user can view all the trips they've bookmarked. Upon page load, the trip items will cascade down to their respective position while fading in. Users are able to delete trips on this page, click on the trips city, country name to view the trip's details and click on the trip they'd like to edit via the edit button on the trip.

![Alt text](wireframes/My_trips_page.png)

## Edit Trip Page

This page allows users to make changes to their previously bookmarked trips. They can change their arrival and departure dates, as well as the attractions they've bookmarked.

There are two windows on this page, one to list their bookmarked attractions and another to list available attractions they haven't bookmarked yet. Upon adding an attraction from the available attractions window, that attraction will jump to the bookmarked attractions window, letting them know their boomark was successful. The opposite happens in the case of removing an attraction from their bookmarked attractions window.

Once the user is done adjusting their trip, they can press the save button in order to commit their changes, upon which they will be redirected back to the My trips page.

![Alt text](wireframes/edit_trips_page.png)

## Trip Details Page

Similar to the trip search result page, this page will show all the details of their trip.

![Alt text](wireframes/Trip_details_page.png)

## Profile Page

A basic Page where the user can see information about themselves. As the focus of the site isn't on user to user interaction, the profile page will instead serve to eventually display data related to the users trips in future updates, to include but is not limited to miles travelled, total trips taken, unlocked accomplishments, and more.

On the profile page, the user is able to click a button that will allow them to edit their profile.

![Alt text](wireframes/Profile_page.png)

## Edit Profile Page

This page shows a form that allows the user to edit various fields related to his profile information.

![Alt text](wireframes/Edit_profile_page.png)
