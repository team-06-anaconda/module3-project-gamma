# Data Models

## Picture Microservice

### Picture
| name     | type   | unique | optional |
| -------- | ------ | ------ | -------- |
| original | string | no     | no       |
| small    | string | no     | no       |

--------------------------------------------------------

## Location Microservice

### LocationIn
| name                   | type    | unique | optional |
| ---------------------- | ------- | ------ | -------- |
| country                | string  | no     | no       |
| city                   | string  | no     | no       |
| population             | integer | no     | no       |
| continent_region       | string  | no     | no       |
| currency_name          | string  | no     | no       |
| capital                | string  | no     | no       |
| tourists               | integer | no     | no       |
| timezone               | string  | no     | no       |
| currency_exchange_rate | string  | no     | no       |
| climate                | string  | no     | no       |
| nearest_airport        | string  | no     | no       |
| airport_city           | string  | no     | no       |
| country_region         | string  | no     | no       |

### Currency
| name | type   | unique | optional |
| ---- | ------ | ------ | -------- |
| code | string | no     | no       |
| name | string | no     | no       |

### Country
| name     | type     | unique | optional |
| -------- | -------- | ------ | -------- |
| currency | currency | no     | no       |
| iso2     | string   | no     | no       |
| capital  | string   | no     | no       |
| tourists | integer  | no     | no       |
| name     | string   | no     | no       |
| region   | string   | no     | no       |

### City
| name       | type    | unique | optional |
| ---------- | ------- | ------ | -------- |
| name       | string  | no     | no       |
| latitude   | string  | no     | no       |
| longitude  | string  | no     | no       |
| population | integer | no     | no       |

### Airport
| name     | type   | unique | optional |
| -------- | ------ | ------ | -------- |
| name     | string | no     | no       |
| city     | string | no     | no       |
| region   | string | no     | no       |
| timezone | string | no     | no       |

### CurrencyExc
| name         | type   | unique | optional |
| ------------ | ------ | ------ | -------- |
| new_currency | string | no     | no       |
| new_amount   | float  | no     | no       |

### Location
| name          | type    | unique | optional |
| ------------- | ------- | ------ | -------- |
| country       | Country | no     | no       |
| city          | City    | no     | no       |
| airport       | Airport | no     | yes      |
| exchange_rate | string  | no     | no       |
| picture_url   | Picture | no     | no       |

--------------------------------------------------------

## Attraction Microservice

### Attraction
| name | type   | unique | optional |
| ---- | ------ | ------ | -------- |
| lat  | string | no     | no       |
| lon  | string | no     | no       |

### POI
| name  | type   | unique | optional |
| ----- | ------ | ------ | -------- |
| name  | string | no     | no       |
| url   | string | no     | yes      |
| phone | string | no     | yes      |

### Address
| name            | type   | unique | optional |
| --------------- | ------ | ------ | -------- |
| freeformAddress | string | no     | no       |

### Results
| name       | type    | unique | optional |
| ---------- | ------- | ------ | -------- |
| poi        | POI     | no     | no       |
| score      | float   | no     | no       |
| address    | Address | no     | no       |
| bookmarked | bool    | no     | yes      |

### AttractionIn
| name   | type    | unique | optional |
| ------ | ------- | ------ | -------- |
| lat    | string  | no     | no       |
| lon    | string  | no     | no       |
| radius | integer | no     | no       |
| limit  | integer | no     | no       |
| query  | string  | no     | no       |

### AttractionOut
| name        | type           | unique | optional |
| ----------- | -------------- | ------ | -------- |
| attractions | list[Results]  | no     | no       |

--------------------------------------------------------

## Weather Microservice

### Location
| name    | type   | unique | optional |
| ------- | ------ | ------ | -------- |
| city    | string | no     | no       |
| country | string | no     | no       |


### WeatherCoord
| name | type   | unique | optional |
| ---- | ------ | ------ | -------- |
| lat  | string | no     | no       |
| lon  | string | no     | no       |

### Weather
| name | type   | unique | optional |
| ---- | ------ | ------ | -------- |
| main | string | no     | no       |

### WeatherMain
| name       | type  | unique | optional |
| ---------- | ----- | ------ | -------- |
| temp       | float | no     | no       |
| feels_like | float | no     | no       |
| pressure   | float | no     | no       |
| humidity   | float | no     | no       |
| temp_min   | float | no     | no       |
| temp_max   | float | no     | no       |

### WeatherWind
| name  | type  | unique | optional |
| ----- | ----- | ------ | -------- |
| speed | float | no     | no       |
| deg   | float | no     | no       |

### WeatherOut
| name    | type          | unique | optional |
| ------- | ------------- | ------ | -------- |
| weather | list[Weather] | no     | no       |
| main    | WeatherMain   | no     | no       |
| wind    | WeatherWind   | no     | no       |

### LocationKeyOut
| name    | type          | unique | optional |
| ------- | ------------- | ------ | -------- |
| Key     | string        | no     | no       |

### MinimumOut
| name    | type          | unique | optional |
| ------- | ------------- | ------ | -------- |
| value   | integer       | no     | no       |
| Unit    | string        | no     | no       |

### MaximumOut
| name    | type          | unique | optional |
| ------- | ------------- | ------ | -------- |
| value   | integer       | no     | no       |
| Unit    | string        | no     | no       |

### TempOut
| name    | type          | unique | optional |
| ------- | ------------- | ------ | -------- |
| Minimum | MinimumOut    | no     | no       |
| Maximum | MaximumOut    | no     | no       |


### DayOut
| name       | type          | unique | optional |
| ---------- | ------------- | ------ | -------- |
| Icon       | integer       | no     | no       |
| IconPhrase | string        | no     | no       |

### NightOut
| name       | type          | unique | optional |
| ---------- | ------------- | ------ | -------- |
| Icon       | integer       | no     | no       |
| IconPhrase | string        | no     | no       |

### DailyForecasts
| name        | type          | unique | optional |
| ----------- | ------------- | ------ | -------- |
| Date        | datetime      | no     | no       |
| Temperature | TempOut       | no     | no       |
| Day         | DayOut        | no     | no       |
| Night       | NightOut      | no     | no       |


### HeadlineOut
| name        | type          | unique | optional |
| ----------- | ------------- | ------ | -------- |
| Text        | string        | no     | no       |
| Link        | string        | no     | no       |

### ForecastOut
| name        | type                 | unique | optional |
| ----------- | -------------------- | ------ | -------- |
| Headline    | HeadlineOut          | no     | no       |
| Temperature | list[DailyForecasts] | no     | no       |

### LocationKeyOut
| name    | type          | unique | optional |
| ------- | ------------- | ------ | -------- |
| Key     | string        | no     | no       |

### MinimumOut
| name    | type          | unique | optional |
| ------- | ------------- | ------ | -------- |
| value   | integer       | no     | no       |
| Unit    | string        | no     | no       |

### MaximumOut
| name    | type          | unique | optional |
| ------- | ------------- | ------ | -------- |
| value   | integer       | no     | no       |
| Unit    | string        | no     | no       |

### TempOut
| name    | type          | unique | optional |
| ------- | ------------- | ------ | -------- |
| Minimum | MinimumOut    | no     | no       |
| Maximum | MaximumOut    | no     | no       |


### DayOut
| name       | type          | unique | optional |
| ---------- | ------------- | ------ | -------- |
| Icon       | integer       | no     | no       |
| IconPhrase | string        | no     | no       |

### NightOut
| name       | type          | unique | optional |
| ---------- | ------------- | ------ | -------- |
| Icon       | integer       | no     | no       |
| IconPhrase | string        | no     | no       |

### DailyForecasts
| name        | type          | unique | optional |
| ----------- | ------------- | ------ | -------- |
| Date        | datetime      | no     | no       |
| Temperature | TempOut       | no     | no       |
| Day         | DayOut        | no     | no       |
| Night       | NightOut      | no     | no       |


### HeadlineOut
| name        | type          | unique | optional |
| ----------- | ------------- | ------ | -------- |
| Text        | string        | no     | no       |
| Link        | string        | no     | no       |

### ForecastOut
| name        | type                 | unique | optional |
| ----------- | -------------------- | ------ | -------- |
| Headline    | HeadlineOut          | no     | no       |
| Temperature | list[DailyForecasts] | no     | no       |

--------------------------------------------------------

## Trips Microservice

### TripIn
| name       | type          | unique | optional |
| ---------- | ------------- | ------ | -------- |
| location   | Location      | no     | no       |
| attraction | AttractionOut | no     | no       |
| arrival    | date          | no     | no       |
| departure  | date          | no     | no       |

### TripOut(TripIn)
| name | type   | unique | optional |
| ---- | ------ | ------ | -------- |
| id   | string | yes    | no       |

### TripList
| name  | type          | unique | optional |
| ----- | ------------- | ------ | -------- |
| trips | list[TripOut] | yes    | no       |

--------------------------------------------------------

## Accounts Microservice

### Account
| name            | type   | unique | optional |
| --------------- | ------ | ------ | -------- |
| id              | string | yes    | no       |
| email           | string | yes    | no       |
| hashed_password | string | no     | no       |

### AccountIn
| name      | type   | unique | optional |
| --------- | ------ | ------ | -------- |
| password  | string | no     | no       |
| password2 | string | no     | no       |
| email     | string | yes    | no       |

### AccountOut
| name  | type   | unique | optional |
| ----- | ------ | ------ | -------- |
| id    | string | yes    | no       |
| email | string | yes    | no       |

### SessionOut
| name       | type   | unique | optional |
| ---------- | ------ | ------ | -------- |
| jti        | string | yes    | no       |
| account_id | string | yes    | no       |

### AccountToken(Token)
| name    | type       | unique | optional |
| ------- | ---------- | ------ | -------- |
| account | AccountOut | yes    | no       |

### AccountForm
| name     | type   | unique | optional |
| -------- | ------ | ------ | -------- |
| username | string | yes    | no       |
| password | string | no     | no       |

### HttpError
| name   | type   | unique | optional |
| ------ | ------ | ------ | -------- |
| detail | string | no     | no       |

### Profile
| name                  | type    | unique | optional |
| --------------------- | ------- | ------ | -------- |
| user_id               | string  | yes    | no       |
| nearest_airport       | string  | no     | yes      |
| can_fly_international | boolean | no     | yes      |
| first_name            | string  | no     | yes      |
| last_name             | string  | no     | yes      |
| birthday              | date    | no     | yes      |
| picture_url           | string  | no     | yes      |
| banner_image_url      | string  | no     | yes      |
| preferred_gender      | string  | no     | yes      |
| about_me              | string  | no     | yes      |

### ProfileIn
| name                  | type    | unique | optional |
| --------------------- | ------- | ------ | -------- |
| nearest_airport       | string  | no     | yes      |
| can_fly_international | boolean | no     | yes      |
| first_name            | string  | no     | yes      |
| last_name             | string  | no     | yes      |
| birthday              | date    | no     | yes      |
| picture_url           | string  | no     | yes      |
| banner_image_url      | string  | no     | yes      |
| preferred_gender      | string  | no     | yes      |
| about_me              | string  | no     | yes      |

### ProfileOut
| name                  | type    | unique | optional |
| --------------------- | ------- | ------ | -------- |
| nearest_airport       | string  | no     | yes      |
| can_fly_international | boolean | no     | yes      |
| first_name            | string  | no     | yes      |
| last_name             | string  | no     | yes      |
| birthday              | string  | no     | yes      |
| picture_url           | string  | no     | yes      |
| banner_image_url      | string  | no     | yes      |
| preferred_gender      | string  | no     | yes      |
| about_me              | string  | no     | yes      |
