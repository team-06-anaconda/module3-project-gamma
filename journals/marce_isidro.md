----Friday, July 28, 2023------
Fixed all esLint compile errors on react load up, reorganized card files, fixed white gap the shows under profile and my trips when the page extends past view port and user has to scroll down, finally fixed glitching with all animations on low-performance computers. Current Animations are now smoother. Reformatted most files under API for an overall cleanup of the code.

----Thursday, July 27, 2023-----
Fixed status 422 error when submitting an empty edit profile form as a new user, changed backend to include default values for a newly created profile. Special data type Date was the cause of the error. Started and finished docs/ghi.md and wireframes for the project. Made loading times for website images faster by loading a low-res version of the image first, then replacing it with a high-res image.

Blockers: still learning framer-motion, but close to being able to fix animation glitching on low-performance computers

----Wednesday, July 26, 2023-----
Completely redesigned edit profile page to look more modern and professional, CSS should be considered complete for edit profile page now and I'm actually satisfied with how it looks. Made Location Cards on home page static so they no longer make calls to the api in order to speed up home page loading time. Animated the Add to Favorite Button and cleaned up some files.

Blockers: There's some bugs and small glitches with the animations. They're not present on faster computers but are noticeable on my slow laptop.

----Tuesday, July 25, 2023----
Incorporated redux-persist to front end. Search slice now rehydrates on refresh using the last search input the user typed in. Incorporated Framer-motion library to project and animated the loading screen in the results page. Added persist actions as ignored checks per redux-persist documentation to fix one of their known bugs.

Blockers: Learn more about framer-motion to add life and more interactibility to the website.


----Monday, July 24, 2023----
Reorganized api files, added login requirements to the rest of the endpoints and created 404 error handling for location endpoints. Updated test files to reflect all changes made to the backend so far.

Blockers: Trying to incorporate redux-persist to project to fix current errors. Everytime the user refreshes on certain pages the require a state variable in redux to be defined (where they are intially set to ""), the web throws multiple errors.


----Thursday, July 20, 2023-----
Cleaned up handleAdd/handleRemove function handlers for bookmarking a trip to be more concise and cleaner, completed functionality for edit profile page. edit profile page now updates api data and redirects to profile page.

Blockers: Redirect is happening too fast, the tag doesn't seem to be getting invalidated to show the new update.


----Wednesday, July 19, 2023----
Added CSS and added more functionality for edit profile page, ran into issue handling checkbox value for unchecking a check box but resolved the issue.


----Tuesday, July 18, 2023----
Re-made most of the tags in the store.js file to more accurately tag cached data and invalidate them, added banner_image_url to api profile model and worked on css for profile and edit profile page.

Blockers: None


----Monday, July 17, 2023----
Fixed flake8 linting errors for api files test_trip and files in trips folder, began work on MyTrips page and trip Details page, finished general layout of both pages.

Blockers: Every time I look at a page I designed, I'm always appalled by how much it looks like a pre-2010 webpage.


----Friday, July 14, 2023----
Created unit tests for weather endpoint and unit tests for create trips endpoint and get all trips endpoint.


----Thursday, July 13, 2023-----
Worked with Aidan to the basic design for the profile page set up, created an edit profile page mutation reducer for profile information, and adjusted the profile repo to be able to accept empty inputs. Incorporated form inputs onto the edit page and collaborated with Aidan to get a basic edit profile page design

Blockers: Need to look up designs for the search locations result page so it'll look more professional.


----Wednesday, July 12, 2023----
Redesigned the entire layout for the profile page, created LocationCards component to display sample locations for the main page and updated get "/locations" backend to be able to take in query parameters.

Blockers: Learn more CSS and designing concepts, profile page looks much better.


----Tuesday, July 11, 2023----
Added simple error message displays for the front-end login and signup, began working on profile page front end and set up query reducer to query for the profile data from the backend api. Finished with the initial layout of the frontend profile page.

Blockers: Need to look up some good designs for a profile page. I'm a little unsatisfied with how it currently looks.


----Monday, July 10, 2023----
Began working on the frontend, implemented redux for authentication endpoints and created the redux slice and api file for logging in and logging out.

Blockers: Figure out how to make an elegant design for the front end.


----Friday, June 29 2023-----
Created models for trips endpoint and implemented trip creation endpoint for the backend. Collaborated with the group to complete the rest of the CRUD endpoints for trips, and set endpoints to require authentication

Blockers: Need to learn Redux in order to incorporate it into the front-end with the user information.


----Thursday, June 28 2023----
planned and made a diagram of the profile creation process with Aidan, collaborated to make the profile endpoints so that a blank profile is automatically created during user creation and made profile endpoints require authorization to use.

Blockers: Figure out how to combine all the data from our endpoints into one single trip for creation.


----Wednesday, June 28 2023----
Worked with Aidan to start and finish the endpoints for locations. Created the endpoints for city, airport and currency exchange data and combined them into one single endpoint at GET "/locations". Added an endpoint to call pexels pictures for the front end for all picture related needs.

Blockers: See if I can refactor the code to make it cleaner. Figure out how we want to do profiles.


----Tuesday, June 27 2023----
Made changes to the account creation process and added error handling for when a user tries to make an account with a username that already exists, and deleted raw password from being saved from the database. Mapped out the process for how we want to pull data from 4 different API's and merge them into the locations model so that we only have to call one endpoint.

Blockers: Need to read the docs for how to call the 4 APIs and structure our models to be able to pull that data in the least painful way


----Monday, June 26 2023----
Integrated mongoDB into the project, set up environment variables for the file. Filled in account models and created the Accountrepo class for making queries to database related to accounts.

Blockers: Completely understand authentication process and see if Sessions need to be saved as part of the authentication process. See if token actually gets deleted and decide if we want a standard whitelist as well.
