07-28-2023: Last day of project time so we made sure everything was cleaned up no console logs and no errors. Blake and Brooks added one last feature on the results page showing a five day forecast with a button that takes you to a 30 day forecast. The api that we used only allows 50 calls per day so we made sure to include a message to the user if that limit is met.

07-27-2023: Brooks did almost all of the work for the data-model.md and then I did the very last part adding the accounts models. Tried adding an add trip button to the my trips page that would allow you to create a new trip directly from the my trips page. I had a blocker I couldn't get the form to submit properly.

07-26-2023: I formatted the date in trips to look a little better than the default date and then I added a function that sorts the dates by arrival date with the arrival dates that come first at the top.

07-25-2023: Today I updated the backend for profiles so now instead of a user inputing their age as a number they input their birthday instead and then their age is displayed on their profile page. It goes off of the current date and subtracts their birthdate, giving back their age in years. I also made it to where after submitting changes on the edit profile page you get redirected back to the profile page. Blocker is making the changed information immediately display on the profile page without having to refresh.

07-24-2023: Marce organized files in the backend and did more research into redux persist. I changed the background pic for the trips page and made the location links underline on hover so you can tell more so that you can click on it and styled the add and remove buttons for the edit trip page as well. Also added real quick the .toLocaleString() method to our larger numbers like population so that the commas are where they need to be. Brooks and Blake were able to make the search feature on the attractions function as intended. I think we are pretty much done we just need to make sure everything is all good.

07-20-2023: Marce finished the functionality of the edit trip page by writing the onSubmit function to allow a user to save the changes to their trip. Went to room 26 where Blake and Brooks were working on the search feature for the attractions. I didn't code anything today.

07-19-2023: Marce finished up the edit trip page styling and then I coded the functionality of the add and remove buttons. The saving the updated trip doesn't work yet.

07-18-2023: We did some more tweaking to the profile page to make it look better and Marce added a way for the user to input a url to change their banner photo on the profile page. Started the edit trip page but haven't completed it yet, will do that tomorrow.

07-17-2023: Marce and I spent the majority of our day making and styling the my trips page. My trips has a functional delete button but the edit button as of now doesn't do anything. Brooks and Blake got the search results page styled and we decided we'd style the trip details page to look like the search results page to keep things consistent.

07-14-2023: We started unit testing today. I started the day off with the testing for attractions and once that was done Marce wrote the code for testing weather. After break Marce started the testing for trips and wrote the code for testing create trip and getting all trips. Once he was done with that we switched and I wrote the code to test getting trip details and updating a trip.

07-13-2023: I was able to finish up the styling for the profile page with Marce's help and then Marce coded the functionality of the edit profile page while I was the observer. We switched off when that was complete and I coded the styling part of the edit profile page.

07-12-2023: Team worked as a group of four to get the main page cards to be able to fetch the data from the backend for the first part of the day. I worked with Marce and made the profile page look a little more presentable. We have a blocker with styling our profile page we can't get the background color on the bottom part of the page to not spill in to the header of the page.

07-11-2023: I did not code anything today. Team worked as a group of four to clean up the front end auth a little bit. Mainly just added the credentials include, invalidatesTags, and the account && so we can show certain things like logout only when a user is logged in and the login/signup when there is no user logged in. I worked with Marce while he coded the profile page, we got it to a point where it is a functional profile page but we are going to make it look a little better as it is a little bland right now.

07-10-2023: Team worked together as a group of 4 for the entire day to complete auth for the front end. Blake did some styling with bootstrap to make the auth pages look a little better and added a gif that we want to have fill up the entire background for the auth pages and the home page. Our blocker is that we cannot figure out how to make the gif fill up the entire screen as a background.

06-30-2023: Team worked together as a group of 4 to complete the endpoints for our trips. I coded the endpoints for updating and deleting a trip. We are now done with the backend of our project and will transition to the front end when we get back from summer break.

06-29-2023: Created endpoints for creating a profile and updating a profile. Added API keys in my .env file for our weather and attractions so I now have all of the API keys I need and tested everything to ensure I can successfully communicate with each one.

06-28-2023: Finished the models for locations and made a repository to fetch the data from our Ninjas Location API. Added my API key to .env file and included it in the yaml file without showing the key itself.

06-27-2023: Started on the models for locations. Team seeked help from Riley while I was in a meeting with Arthur and he told them to do our API's like he showed us. Team agreed to watch week 13 day 2 FastAPI Q&A on our own so we can be better prepared for tomorrow.

06-26-2023: I didn't code anything today. Team worked as a group of 4 to complete backend auth.
