7/28
Yesterday- we improved the loading time of the site by reversing the order that pictures are loaded. By loading the small (low res) image first, we can see more instant results, while just the image finishes loading
Today- run formatting commands to clean code, run unit tests to make sure they still pass, remove any errors. Edited the styling of the modal. Added a popping effect to the css on edit trips and on results page.
Blocker-none

7/27
Today- we are fixing linting errors, checking for misspellings, and filling in the docs/readme. CSS files are better organized. More styling of the results page, and we are adding the forecast to it also.
Blockers- 422 error trying to get the forecast working, very limited amount of api calls available with the free account

7/26
Today- we added a gradient background based off the main page to the profile and results page, also added current weather to the results, and fixed sizing of columns. Added icons for the results on the results page. Editing of the edit profile page including colors, placement and sizing.
Blockers- None

7/25
Today -we added a method on some data to show commas (styling) on th results page, also added persist to save the search if you reload the page. Working on CSS formatting and styling.
Blockers- None

7/24
Today we are working on the results page, implementing the filter slice and using a use-state variable for the filtered results. We added the error handling with try/except blocks on the backend
Blockers; flex-box is snapping the search bar in different places due to query length

7/20
Today we refine the function of the search bar within the results page, edit profile page is completed.
Blockers; placement of the search bar

7/19
We added a filter for exchange rates in the us, tweaked a few css aspects, and added the search bar on
results page for specific queries
Blockers;none

7/18
Today we did more work on the main page and the results page.
Blockers; none
Tomorrow; refine address data and exchange rate data

7/17
Today we worked on the results page after searching for a location. the attractions now display, and the base search is for tours in that city.
Blockers; trouble with trying to format and resize
Tomorrow we will work on a filter based on country to format the address better

7/14
Today we wrote the unit tests for all the query endpoints for the frontend. we are working on adding the attractions to the results page also
Blockers-figuring out the right way to pass the data to be viewed on the results page

7/13
Today we worked on the search feature on the frontend.
Blockers; had some trouble making api calls and using the data on the results page

7/12
Today we finished up the main page styling
Blockers-none

7/11
We worked on the front end for the main page and the profiles today
Blockers; none

7/10
Today, we added the frontend react and redux for the login, logout, and signup page.
Blockers; added a video background for the homepage but it wont fullscreen as the first(base) layer of page.

6/30
Today we added the trip, delete,edit endpoints for the whole page, finishing up most of the backend.
Blockers; none

6/29
Today we made the endpoints for weather api, which uses city and country to grab coordinates and show related weather.
Blockers; na
Next working on trips endpoint

6/28
Finished using fastapi to get coordinates using our new attraction api since yesterday.
Blockers; n/a.
Tomorrow working on the optional parameters for the attractions, and the weather

6/27/23
Today we are working on the login logout feature and possibly some other modules such as react-frontend and attractions or locations.
Blockers; none
We ended up creating the endpoints for the location and related weather data
Tomorrow working on attractions
