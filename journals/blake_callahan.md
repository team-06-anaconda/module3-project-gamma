## Blakes Journal
07/28/2023 - Finished front-end aspect of forecast api to get 5 day forecast and link to 30+ day forecast for that area. Added styling to Forecast component. Project is ready for submission. Further stretch goals: adding a specific attraction to list of favorite attractions, styling each button using Framer-motion, and allowing a user to add an image to profile page from their computer.
07/27/2023 - Completed README and docs. added forecast api fetch in backend, ran out of free calls before I could finish displaying it in front end.
    Blockers: None
    Tomorrow: finish git stuff and finilize forecast
07/26/2023 - Main page finalized, added functionality to click on card and navigate to results page based off of that cards city & country.
    Blockers: None
    Tomorrow: Style, format code, etc.
07/25/2023 - Results page finalized. Added current weather information for city in question. home page is finalized opther than possible stretch goals.
    Blockers: None
    Tomorrow: Style/loose ends
07/24/2023 - Finished attractions filter by query using a frontend api call and render it live using slice(thanks Marce).
    Blockers: None
    Tomorrow: Style
07/20/2023 - Started on attractions filter by query. did not finish.
    Blockers: Ran out of time/patience.
    Monday: continue on this challenge
07/19/2023 - Favorite button works and saves to myTrips.
    Blockers: None
    Tomorrow: styling across the board
07/18/2023 - More styling for homepage, navbar, and search results page. added favorite component for adding the result to
    MyTrips but didnt finish.
    Blockers: didnt finish linking backend to frontend
    Tomorrow: continue with favorite button functionality
07/17/2023 - Styling and functionality for results page satisfactory. search page still needs some eyecatching features to
    engage users.
    Blockers: None
    Tomorrow: implement favorite button so trips can be favorited through UI. Also need to change address for locations out of US to display more usable information.

07/14/2023 - Wrote unit tests for weather api and got attractions data in results.
    Blockers: None
    Monday: finish styling results page

07/13/2023 - Results page gets data from external api and displays it in devTools. thank you teammates.
    Blockers: None
    Tomorrow: integrate a method to display that fetched data on the results page

07/12/2023 - Finished home page yet ran out of time to get a decent start on search page. home page looks great though.
    Blockers: None
    Tomorrow we will finish search page functionality and styling.

07/11/2023 - Started working on home page functionality and styling.
    Blockers: None
    Tomorrow we plan on finishing home page and beginning search page.

07/10/2023 - Started front end authorization (log in, log out, signup) and styling.
    Blockers: Cannot get this video formatted properly.
    Tomorrow we will continue frontend.

06/30/2023 - Finished backend moving to frontend after break.
    Blockers: None
    Monday: Front end Auth

06/29/2023 - Attractions endpoint and weather are completed; needs to be integrated so that only users can access.
    Blockers: None
    Tomorrow we plan on implementing  a "trips" functionality to save all endpoints  (attractions, locations, weather, and picture) to a trip. one user can have many trips. 30 day forecast is now a stretch goal.

06/28/2023 - Completed api for coordinates and api for attractions within a givin radius.
    Blockers: None
    Tomorrow we plan on implementing 'query', 'limit', and 'radius' fields on attraction api to be optional fields. Also plan on implementing api call to get current weather and 30 day forecast.

06/27/2023 - Worked on external api model for getting latitude and longitude.
    Blockers: trying to get what i just said to work. may take some time.
    Tomorrow i plan on being able to submit a city and country and get a latitude and longitude through FASTApi.

06/26/2023 - Added login and logout functionality (backend); authentication token able to be saved and removed to database upon login and logout, respectively.
    Blockers: None.
    Tomorrow we plan on implementing a simple front-end login and logout as well as starting on other models.
