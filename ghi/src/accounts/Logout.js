import { useLogoutMutation } from "../store/tripOutAPI";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";

function Logout () {
    const navigate = useNavigate()
    const [logout, result] = useLogoutMutation()

    const handleClick = () => {
        logout()
    }

    useEffect(() => {
        if (result.isSuccess){
            navigate("/")
        }
    }, [result, navigate])

    return (
        <>
            <div className="container text-center">
                <button type="submit" className="btn btn-outline-danger text-white" onClick={handleClick}>Logout</button>
            </div>
        </>
    )
}

export default Logout;
