
import {useState, useEffect, React} from 'react';
import { useNavigate } from "react-router-dom";
import { useSignupMutation } from "../store/tripOutAPI";
import "./css/accounts.css"
import video from "./css/signupbg.webm";


export default function SignUp() {
    const navigate = useNavigate()
    const [signUpData, setSignUpData] = useState({
        email: "",
        password: "",
        password2: "",
    });
    const [signUp, result] = useSignupMutation()

    const changeHandler = event => {
        setSignUpData({...signUpData, [event.target.name]:event.target.value})
    }
    const submitHandler = async event => {
        event.preventDefault()
        signUp(signUpData)
    }

    useEffect(() => {
        if (result){
          if (result.isSuccess){
            navigate("/")
          }
        }
    }, [result, navigate])

    return(
    <div className="main">
      <link rel="stylesheet" href="./video/style/account.css" />
      <video autoPlay loop muted playsInline>
        <source src={video} type="video/webm"/>
      </video>
      <div className="position-absolute top-50 start-50 translate-middle container py-5 h-100">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col-12 col-md-8 col-lg-6 col-xl-5">
            <div className="bg-transparent text-light" >
              <div className="card-body p-5 text-center">
                <div className="mb-md-5 mt-md-4 pb-5">
                  <h2 className="fw-bold mb-2 text-uppercase">Sign Up</h2>
                  <p></p>
                  <form onSubmit={submitHandler}>
                    <div className="form-outline form-white mb-4">
                        <label htmlFor="Email1" className="form-label"></label>
                        <input id="Email1" className="form-control form-control-lg" placeholder="Email" aria-describedby="emailHelp" type="email" name="email" value={signUpData.email} onChange={changeHandler}/>
                    </div>
                    <div className="form-outline form-white mb-4">
                        <label htmlFor="Password1" className="form-label"></label>
                        <input id="Password1" className="form-control form-control-lg" placeholder="Password" type="password" name="password" value={signUpData.password} onChange={changeHandler}/>
                    </div>
                    <div className="form-outline form-white mb-4">
                        <label htmlFor="Password2" className="form-label"></label>
                        <input id="Password2" className="form-control form-control-lg" placeholder="Confirm Password" type="password" name="password2" value={signUpData.password2} onChange={changeHandler}/>
                        <div id="passwordHelp" className="form-text" style={{color:"#8f8a8a"}}>We'll never share your information with anyone else.</div>
                    </div>
                    <div>
                        <button className="signup_custom_btn btn btn-dark btn-lg border-3" type="submit">Create Account!</button>
                    </div>
                    {result.isError &&
                      <p style={{color:"white"}}>{result.error.data.detail}</p>
                    }
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
