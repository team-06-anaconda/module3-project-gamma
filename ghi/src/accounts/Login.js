import { useState, useEffect, React} from 'react';
import { useNavigate } from 'react-router-dom';
import { useLoginMutation } from '../store/tripOutAPI';
import video from "./css/loginbg.webm";

export default function Login(props) {
  const [login, result] = useLoginMutation()
  const navigate = useNavigate()
  const [loginData, setLoginData] = useState({
        email: "",
        password: ""
    });

  const changeHandler = event => {
      setLoginData({...loginData, [event.target.name]:[event.target.value]})
  }

  const submitHandler = async event => {
      event.preventDefault();
      const data = new FormData()
      data.append('username', loginData.email)
      data.append('password', loginData.password)
      login(data)
  }

  useEffect(() => {
    if (result){
      if (result.isSuccess){
        navigate("/")
      }
    }
  }, [result, navigate])

  return(
    <div className="main">
      <link rel="stylesheet" href="./video/style/account.css" />
      <video autoPlay loop muted playsInline>
        <source src={video} type="video/webm"/>
      </video>
      <div className="position-absolute top-50 start-50 translate-middle container py-5 h-100">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col-12 col-md-8 col-lg-6 col-xl-5">
            <div className="bg-transparent text-dark" >
              <div className="card-body p-5 text-center">
                <div className="mb-md-5 mt-md-4 pb-5">
                  <h2 className="fw-bold mb-2 text-uppercase">Login</h2>
                  <p className="text-dark-50 mb-5">Please enter your email and password!</p>
                  <form onSubmit={submitHandler}>
                    <div className="form-outline form-white mb-4">
                      <input type="email" id="typeEmailX" placeholder="Email" className="form-control form-control-lg" name="email" value={loginData.email} onChange={changeHandler} />
                    </div>
                    <div className="form-outline form-white mb-4">
                      <input type="password" id="typePasswordX" placeholder="Password" className="form-control form-control-lg" name="password" value={loginData.password} onChange={changeHandler} />
                    </div>
                    <div>
                      <button className="btn btn-dark" type="submit">Submit</button>
                    </div>
                    {result.isError &&
                      <p style={{color:"white"}}>{result.error.data.detail}</p>
                    }
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
