import { useGetForecastsQuery } from "../store/tripOutAPI";
import { useSelector } from "react-redux";
import "./css/cards.css";


export default function Forecasts() {
  const location = useSelector((state) => state.search);
  const {
    data: forecasts,
    isLoading,
    isError,
    error,
  } = useGetForecastsQuery(location);

  function formatDate(date) {
    const format = { month: "long", day: "2-digit", year: "numeric" };
    const formattedDate = new Date(date).toLocaleDateString("en-US", format);
    const [month, day, year] = formattedDate.split(" ");
    return `${month} ${day} ${year}`;
  }

  const handleMouseMove = event => {
      for (const card of document.getElementsByClassName("custom-card")){
          const rect = card.getBoundingClientRect()
          const x = event.clientX - rect.left
          const y = event.clientY - rect.top

          card.style.setProperty("--mouseX", `${x}px`)
          card.style.setProperty("--mouseY", `${y}px`)
      }
  }

  if (isLoading) {
    return (
      <div className="card mx-1 bg-transparent border-0">
        <div className="position-absolute top-50 start-50 translate-middle">
          <div className="card-body ">
            <div
              className="spinner-border"
              style={{ width: "3rem", height: "3rem" }}
              role="status"
            >
              <span className="visually-hidden">Loading...</span>
            </div>
          </div>
        </div>
      </div>
    );
  }

  return (
    <>
      {forecasts && (
        <>
          <div className="mb-3 text-center">{forecasts.Headline.Text}</div>
          <div className="custom-card-box" onMouseMove={handleMouseMove}>
            {forecasts.DailyForecasts.map((forecast, index) => {
              return (
                <div className="custom-card" key={index}>
                  <div className="custom-card-border"></div>
                  <div className="custom-card-content">
                    <div className="d-flex w-100 justify-content-center">
                      <h5 className="mb-1">{formatDate(forecast.Date)}</h5>
                    </div>
                    <div className="d-flex row">
                      <div className="col d-flex justify-content-center">
                        <h5 className="mb-1">
                          Low - {forecast.Temperature.Minimum.Value}° F
                        </h5>
                      </div>
                      <div className="col d-flex justify-content-center">
                        <h5 className="mb-1">
                          Day - {forecast.Day.IconPhrase}
                        </h5>
                      </div>
                    </div>
                    <div className="d-flex row">
                      <div className="col d-flex justify-content-center">
                        <h5 className="mb-1">
                          High - {forecast.Temperature.Maximum.Value}° F
                        </h5>
                      </div>
                      <div className="col d-flex justify-content-center">
                        <h5 className="mb-1">
                          Night - {forecast.Night.IconPhrase}
                        </h5>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
            <div className="d-flex justify-content-center">
              <a
                className="btn btn-outline-light btn-lg"
                href={forecasts.Headline.Link}
                target="_blank"
                rel="noreferrer"
              >
                Open 30-Day Forecast
              </a>
            </div>
          </div>
        </>
      )}
      {isError && (
        <div>
          <p>{error?.data.detail}</p>
        </div>
      )}
    </>
  );
}
