import {useState} from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SetSearch } from "../store/searchSlice";
import "./css/cards.css";


export default function Search() {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const [searchData, setSearchData] = useState({
        city: "",
        country: ""
    })

    const changeHandler = event => {
        setSearchData({...searchData, [event.target.name]: event.target.value})
    }

    const submitHandler = (event) => {
        event.preventDefault();
        dispatch(SetSearch(searchData));
        navigate("/trips/results")

    };

    return (
        <>
            <form onSubmit={submitHandler}>
                <div className="row">
                    <div className="col">
                        <input type="text"
                            className="form-control bg-transparent"
                            placeholder="City"
                            name="city"
                            value={searchData.city}
                            onChange={changeHandler}
                        />
                    </div>
                    <div className="col">
                        <input type="text"
                            className="form-control bg-transparent"
                            placeholder="Country" name="country"
                            value={searchData.country}
                            onChange={changeHandler}
                        />
                    </div>
                    <div className="col">
                        <button type="submit" className="search_custom_btn btn btn-outline-dark">Search</button>
                    </div>
                </div>
            </form>
        </>
    )
};
