import {
    useCreateTripMutation,
    useFilterAttractionsQuery,
    useGetLocationQuery
} from "../store/tripOutAPI";
import { useSelector} from "react-redux";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import {motion} from "framer-motion";
import {default as planeIcon} from "./css/svgs/planeIcon.svg";

export default function Favorite() {
    const location = useSelector((state) => state.search)
    const query = useSelector((state) => state.filter)
    const {data: attractions} = useFilterAttractionsQuery({location: location, query: query.query})
    const {data: locationData} = useGetLocationQuery(location)
    const navigate = useNavigate();
    const [hovering, setHovering] = useState(false)
    const [clicked, setClicked] = useState(false)

    const [dates, setDates] = useState({
        arrival: "",
        departure: ""
    })

    const changeHandler = event => {
        setDates({...dates, [event.target.name]: event.target.value})
    }

    const [createTrip, results] = useCreateTripMutation()
    const formData = {
        "location": locationData,
        "attraction": attractions,
        "arrival": dates.arrival,
        "departure": dates.departure
    }

    useEffect(() => {
        if(results.isError){
            navigate("/error")
        }
        if (results.isSuccess){
            navigate("/trips/mytrips")
        }
    },[results, navigate])

    return (
        <>
            <div style={{overflow:"hidden", borderRadius:"5px"}}>
                <motion.button
                    type="button"
                    data-bs-toggle="modal"
                    data-bs-target="#favoritesModal"
                    style={{
                        display: "flex",
                        alignItems: "center",
                        border:"1px solid black",
                        width:"9em"
                    }}
                    className="btn"
                    onHoverStart={() => setHovering(true)}
                    onHoverEnd={() => setHovering(false)}
                    onClick={()=> setClicked(true)}
                >
                    Add to My Trips
                </motion.button>
                <motion.button
                    type="button"
                    data-bs-toggle="modal"
                    data-bs-target="#favoritesModal"
                    style={{
                        display: "flex",
                        alignItems: "center",
                        marginTop:"-2.3em",
                        backgroundColor:"black",
                        height:"2.3em",
                        width:"9em"
                    }}
                    className="btn btn-dark"
                    animate={(hovering||clicked)?{
                        x:0,
                        z:100
                    }: {}}
                    initial={{
                        x:150
                    }}
                    transition={{
                        duration: 0.7
                    }}
                    onHoverStart={() => setHovering(true)}
                    onHoverEnd={() => setHovering(false)}
                    onClick={()=> setClicked(true)}
                >
                    <img className="mx-auto" src={planeIcon} alt="Add to My trips"/>
                </motion.button>
            </div>
            <div>
                <div className="modal fade"
                    id="favoritesModal"
                    tabIndex="-1"
                    aria-labelledby="exampleModalLabel"
                    aria-hidden="true"
                    onBlur={()=>{setClicked(false)}}
                >
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content modalForm text-white">
                            <div className="modal-header">
                                <h1 className="modal-title fs-5"
                                    id="favoritesModalLabel"
                                >
                                    Enter your arrival and departure dates!
                                </h1>
                                <button type="button"
                                    className="btn-close"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                    onClick={() => {setClicked(false)}}
                                >
                                </button>
                            </div>
                            <div className="modal-body">
                                <div>
                                    <div className="mb-3" >
                                        <label htmlFor="formGroupExampleInput" className="form-label">Arrival</label>
                                        <input type="date"
                                            className="form-control"
                                            id="formGroupExampleInput"
                                            placeholder="Arrival"
                                            name="arrival"
                                            value={dates.arrival}
                                            onChange={changeHandler}
                                            required
                                        />
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="formGroupExampleInput2" className="form-label">Departure</label>
                                        <input type="date"
                                            className="form-control"
                                            id="formGroupExampleInput2"
                                            placeholder="Departure"
                                            name="departure"
                                            value={dates.departure}
                                            onChange={changeHandler}
                                            required
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button"
                                    className="btn btn-outline-dark text-white"
                                    data-bs-dismiss="modal"
                                    onClick={() => setClicked(false)}
                                >
                                    Close
                                </button>
                                <button type="button"
                                    className="btn btn-outline-light"
                                    data-bs-dismiss="modal"
                                    onClick={() => {createTrip(formData)}}
                                >
                                    Save Dates
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
