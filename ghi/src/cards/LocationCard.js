import { useGetLocationQuery } from "../store/tripOutAPI";

function LocationCard(props) {
    const {data: location, isLoading} = useGetLocationQuery(props.location)

    if (isLoading){
        return (
            <>
                <div className="card mx-1 rounded-4 mb-5 bg-dark text-white bg-opacity-25 text-emphasis-dark border-0 rounded">
                    <div className="">
                        <p className="card-img-top placeholder-glow">
                            <span className="placeholder col-12 rounded-4" style={{height:"300px"}}/>
                        </p>
                        <div className="card-body bg-transparent bg-opacity-25 rounded-4 mt-1" >
                            <h5 className="card-title placeholder-glow">
                                <span className="placeholder col-6"/>
                            </h5>
                            <p className="card-text placeholder-glow">
                                <span className="mt-2 mb-2 placeholder col-7"/>
                                <span className="mt-2 mb-2 placeholder col-3"/>
                                <span className="mt-2 mb-2 placeholder col-4"/>
                                <span className="mt-2 mb-2 placeholder col-7"/>
                                <span className="mt-2 mb-2 placeholder col-8"/>
                                <span className="mt-2 mb-2 placeholder col-8"/>
                            </p>
                        </div>
                    </div>
                </div>
            </>
        )
    }

    return (
        <div className="card mx-1 rounded-4 mb-5 bg-dark text-white bg-opacity-25 text-emphasis-dark border-0 rounded">
            <div className="">
                <img
                src={location.picture_url.original}
                className="card-img-top rounded-4"
                alt="locationImage"
                />
                <div className="card-body bg-transparent bg-opacity-25 rounded-4 mt-1" >
                    <h5 className="card-title">{`${location.city.name}, ${location.country.name}`}</h5>
                    <p className="card-text">Population: {location.city.population.toLocaleString()}</p>
                    <p className="card-text">Nearest Airport: {location.airport.name}</p>
                    <p className="card-text">Exchange Rate: {location.exchange_rate}</p>
                    <p className="card-text">Annual Tourists: {location.country.tourists.toLocaleString()}</p>
                </div>
            </div>
        </div>
    )
}

export default LocationCard;
