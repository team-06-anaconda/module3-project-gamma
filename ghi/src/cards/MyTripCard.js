import {useState} from 'react';
import { motion } from 'framer-motion';
import { NavLink } from 'react-router-dom';
import { useDeleteTripMutation } from '../store/tripOutAPI';


function MyTripCard (props) {
    const [loading, setLoading] = useState(true)
    const [highResLoad, setHighresLoad] = useState(false)
    const [hovering, setHovering] = useState(false)
    const [deleteTrip] = useDeleteTripMutation()

    const trip = props.trip
    const index = props.index? props.index: 0;

    function formatDate(date) {
        const format = { month: "long", day: "2-digit", year: "numeric" }
        const formattedDate = new Date(date).toLocaleDateString("en-US", format)
        const [month, day, year] = formattedDate.split(' ')
        return `${month} ${day} ${year}`
    }

    return (
        <motion.div
            animate={!loading?{
                y: 0,
                opacity:1
            }: {}}
            initial={{
                y: -50,
                opacity:0
            }}
            transition={{
                type:"spring",
                stiffness:20,
                delay: (index+2)*0.2
            }}
            onAnimationComplete={() => setHighresLoad(true)}
        >
            <motion.div
                className="container mt-3 mb-3 position-relative border border-dark rounded-4 trip-items"
                animate={!loading?{
                    scale: hovering? 1.03: 1
                }: {}}
                onHoverStart={() => {setHovering(true)}}
                onHoverEnd={() => {setHovering(false)}}
            >
                <div className="row">
                    <div className="col-xl-3 col-md-12 p-0">
                        <img
                            className="w-100 h-100 rounded-start-4"
                            src={trip.location.picture_url.small}
                            alt="some pic"
                            onLoad={() => setLoading(false)}
                            style={highResLoad?{display:"none"}:{}}
                        />
                        <img
                            className="w-100 h-100 rounded-start-4"
                            src={trip.location.picture_url.original}
                            alt="some pic"
                            style={highResLoad?{}:{display:"none"}}
                        />
                    </div>
                    <div className="col-xl-7 col-md-12 pt-2 fs-4 ps-4">
                        <NavLink className="custom-title" to={`/trips/${trip.id}`}><h1>{trip.location.city.name}, {trip.location.country.name}</h1></NavLink>
                        <p className="pt-2 mb-0">Population: {trip.location.city.population.toLocaleString()}</p>
                        <p className="mb-0">Arrival: {formatDate(trip.arrival)}</p>
                        <p className="mb-0">Departure: {formatDate(trip.departure)}</p>
                        <p className="mb-3">TimeZone: {trip.location.airport?.timezone}</p>
                    </div>
                    <div>
                        <div className="position-absolute bottom-0 end-0 mb-3 me-3">
                            <NavLink to={`/trips/${trip.id}/edit`} className="btn btn-outline-success me-2 custom">Edit</NavLink>
                            <button className="btn btn-outline-danger" onClick={() => deleteTrip(trip.id)}>Delete</button>
                        </div>
                    </div>
                </div>
            </motion.div>
        </motion.div>
    )
}

export default MyTripCard;
