import { useGetWeatherQuery } from "../store/tripOutAPI";
import { useSelector } from "react-redux";
import Forecasts from "./ForecastsCard";
import { useState } from "react";

export default function Weather() {
  const location = useSelector((state) => state.search);
  const { data: weather, isLoading } = useGetWeatherQuery(location);
  const [showButton, setShowButton] = useState(false);
  const toUpper = location.city[0].toUpperCase() + location.city.slice(1);

  if (isLoading) {
    return (
      <div className="card mx-1 bg-transparent border-0">
        <div className="position-absolute top-50 start-50 translate-middle">
          <div className="card-body ">
            <div
              className="spinner-border"
              style={{ width: "3rem", height: "3rem" }}
              role="status"
            >
              <span className="visually-hidden">Loading...</span>
            </div>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div>
      <div className="card bg-dark bg-opacity-75 border-warning text-light">
        <div className="card-body">
          <h5 className="card-title">Current Weather in {toUpper}:</h5>
          <h6 className="card-subtitle mb-2 font">{weather.weather[0].main}</h6>
          <div className="row font">
            <div className="col">
              <div>
                Feels like: {weather.main.feels_like}° F
                <br />
                High: {weather.main.temp_max}° F
                <br />
                Low: {weather.main.temp_min}° F
              </div>
            </div>
            <div className="col">
              Humidity: {weather.main.humidity}%
              <br />
              Pressure: {weather.main.pressure} hPa
              <br />
              Wind: {weather.wind.speed} mph @ {weather.wind.deg}°
            </div>
          </div>
          {showButton && (
            <div className="d-flex justify-content-center mt-3">
              <button
                type="button"
                className="btn btn-outline-success text-white"
                data-bs-toggle="modal"
                data-bs-target="#forecastsModal"
              >
                5 Day Forecast
              </button>
            </div>
          )}
          {!showButton && (
            <div className="d-flex justify-content-center mt-3">
              <button
                type="button"
                className="btn btn-outline-light"
                onClick={() => setShowButton(true)}
              >
                Get Forecast Information
              </button>
            </div>
          )}
        </div>
      </div>
      {showButton && (
        <div className="modal-dialog modal-dialog-centered">
          <div
            className="modal fade"
            id="forecastsModal"
            tabIndex="-1"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-lg custom-modal-width">
              <div className="modal-content bg-dark">
                <div className="modal-header">
                  <h1 className="modal-title fs-5" id="exampleModalLabel">
                    5 Day Forecast
                  </h1>
                  <button
                    type="button"
                    className="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    onClick={() => setShowButton(false)}
                  ></button>
                </div>
                <div className="modal-body bg-dark">
                  <Forecasts />
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-bs-dismiss="modal"
                    onClick={() => setShowButton(false)}
                  >
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
