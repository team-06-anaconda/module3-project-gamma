import { useState} from 'react';
import { useDispatch } from 'react-redux';
import { SetFilter } from '../store/filterSlice';


export default function Filter() {
  const dispatch = useDispatch();
  const [query, setQuery] = useState({
    query: '',
  });

  const changeHandler = (event) => {
    setQuery({ ...query, [event.target.name]: event.target.value });
  };

  const submitHandler = async (event) => {
    event.preventDefault();
    dispatch(SetFilter(query))
  };

  return (
    <>
      <div>
        <div>
          <form onSubmit={submitHandler} style={{ display: "flex", alignItems: "center" }}>
            <label className="visually-hidden" htmlFor="autoSizingInput">
              Name
            </label>
            <input
              type="text"
              className="form-control border-bottom border-dark me-3"
              id="autoSizingInput"
              name="query"
              value={query.query}
              onChange={changeHandler}
              placeholder="Search e.g. Food">
            </input>
            <button type="submit" className="btn btn-outline-dark">
              Search
            </button>
          </form>
        </div>
      </div>
    </>
  );
}
