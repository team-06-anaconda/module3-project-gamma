import {madrid, nassau, tokyo} from "./imgs/_components";

export const homePageLocations=[
    {
        "country": {
            "currency": {
                "code": "JPY",
                "name": "Yen"
            },
            "iso2": "JP",
            "capital": "Tokyo",
            "tourists": 31192,
            "name": "Japan",
            "region": "Eastern Asia"
        },
        "city": {
            "name": "Tokyo",
            "latitude": "35.6897",
            "longitude": "139.692",
            "population": 37977000
        },
        "airport": {
            "name": "Narita International Airport",
            "city": "Tokyo",
            "region": "Chiba",
            "timezone": "Asia/Tokyo"
        },
        "exchange_rate": "1$ = 140.2 JPY",
        "picture_url": {
            "original": tokyo,
            "small": "https://images.pexels.com/photos/2614818/pexels-photo-2614818.jpeg?auto=compress&cs=tinysrgb&h=130"
        }
    },
    {
        "country": {
            "currency": {
                "code": "EUR",
                "name": "Euro"
            },
            "iso2": "ES",
            "capital": "Madrid",
            "tourists": 82773,
            "name": "Spain",
            "region": "Southern Europe"
        },
        "city": {
            "name": "Madrid",
            "latitude": "40.4189",
            "longitude": "-3.6919",
            "population": 3266126
        },
        "airport": {
            "name": "Cuatro Vientos Airport",
            "city": "Madrid",
            "region": "Madrid",
            "timezone": "Europe/Madrid"
        },
        "exchange_rate": "1$ = 0.9 EUR",
        "picture_url": {
            "original": madrid,
            "small": "https://images.pexels.com/photos/670261/pexels-photo-670261.jpeg?auto=compress&cs=tinysrgb&h=130"
        }
    },
    {
        "country": {
            "currency": {
            "code": "BSD",
            "name": "Bahamian Dollar"
            },
            "iso2": "BS",
            "capital": "Nassau",
            "tourists": 1633,
            "name": "Bahamas",
            "region": "Caribbean"
        },
        "city": {
            "name": "Nassau",
            "latitude": "25.0667",
            "longitude": "-77.3333",
            "population": 274400
        },
        "airport": {
            "name": "Lynden Pindling International Airport",
            "city": "Nassau",
            "region": "New-Providence",
            "timezone": "America/Nassau"
        },
        "exchange_rate": "1$ = 1.0 BSD",
        "picture_url": {
            "original": nassau,
            "small": "https://images.pexels.com/photos/2598721/pexels-photo-2598721.jpeg?auto=compress&cs=tinysrgb&h=130"
        }
    },
]
