import {phone, link, address} from "./css/svgs/_svgs.js";
import { useState } from "react";
import { motion } from "framer-motion";

function AttractionCard(props){
    const attraction = props.attraction
    const [hovering, setHovering] = useState(false)

    return (
        <motion.div
            animate={{
                scale: hovering? 1.03 : 1
            }}
            onHoverStart={() => setHovering(true)}
            onHoverEnd={() => setHovering(false)}
        >
            <ul className="list-group list-group-flush">
                <li className="list-group-item bg-transparent">
                <div className="card attractions-card1 text-white shadow">
                    <div className="card-body ">
                        <h5 className="card-title">
                            {attraction.poi.name}
                        </h5>
                        <div className="card-body">
                            <div className="card-text">
                            <img className="mb-1 me-2" style={{marginLeft:"-7px"}} src={phone} alt="Phone:" width="20em" height="20em"/>
                            <small className="card-text m-0 fs-6">{attraction.poi.phone? attraction.poi.phone: "none"}</small>
                            </div>
                            <div className="card-text">
                            <img className="mb-1 me-2" style={{marginLeft:"-7px"}} src={link} alt="Link:" width="20em" height="20em"/>
                            <a className="text-white"
                                style={{textDecoration:"none"}}
                                href={`https://${attraction.poi.url}`}
                                target="_blank"
                                rel="noreferrer"
                                draggable={false}
                            >
                                <small className="card-text m-0 fs-6">{attraction.poi.url? attraction.poi.url: "none"}</small>
                            </a>
                            </div>
                            <div className="card-text">
                            <img className="mb-1 me-2" style={{marginLeft:"-7px"}} src={address} alt="Address:" width="20em" height="20em"/>
                            <small className="card-text m-0 fs-6">{attraction.address.freeformAddress? attraction.address.freeformAddress: "none"}</small>
                            </div>
                        </div>
                    </div>
                </div>
                </li>
            </ul>
        </motion.div>
    )
}

export default AttractionCard;
