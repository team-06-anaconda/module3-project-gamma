import { NavLink } from "react-router-dom";
import {
  useGetAccountQuery,
  useGetProfileQuery } from "./store/tripOutAPI";
import Logout from "./accounts/Logout";
import "./index.css";

function Nav() {
  const { data: account } = useGetAccountQuery();
  const { data: profile } = useGetProfileQuery();

  return (
    <nav className="navbar navbar-expand-lg navbar-dark sticky-top border-bottom border-dark bg-dark">
        <div className="container-fluid">
            <button className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0 flex-wrap">
                    <li className="nav-item pt-1">
                        <NavLink className="navbar-brand" to="/">
                            Trip Out
                        </NavLink>
                    </li>
                </ul>
            </div>
                { account &&
                    <div className="d-flex flex-row-reverse">
                        <div className="dropdown">
                            <button type="button"
                                className="btn btn-outline-dark border-0 text-white dropdown-toggle"
                                data-bs-toggle="dropdown"
                                aria-expanded="false"
                            >
                                {profile?.first_name? profile.first_name: account.email}
                            </button>
                            <ul className="dropdown-menu dropdown-menu-end border-dark bg-dark text-white container text-center">
                                <li>
                                    <NavLink className="dropdown-item bg-dark text-white" to="/">
                                        Home
                                    </NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="dropdown-item bg-dark text-white" to="/trips/mytrips">
                                        My Trips
                                    </NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="dropdown-item bg-dark text-white" to="/accounts/profile">
                                        My Profile
                                    </NavLink>
                                </li>
                                <li>
                                    <hr className="dropdown-divider"/>
                                    <NavLink className="dropdown-item bg-dark text-white p-2">
                                        <Logout/>
                                    </NavLink>
                                </li>
                            </ul>
                        </div>
                    </div>
                }
                {!account &&
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <NavLink className="nav-link bg-dark text-white" to="/login">
                                Log In
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link bg-dark text-white" to="/signup">
                                Sign Up
                            </NavLink>
                        </li>
                    </ul>
                }
        </div>
    </nav>
  );
}

export default Nav;
