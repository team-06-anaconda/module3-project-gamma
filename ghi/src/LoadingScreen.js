import {motion} from "framer-motion";

function LoadingScreen() {
    return (
        <motion.div
            key="loader"
            exit={{
                opacity:0,
                transition:{
                    duration: 0.2,
                    delay:1.7
                }
            }}
            className="container-fluid"
            style={{height:"93vh",overflow:"hidden", backgroundColor:"black", position:"fixed", zIndex:"100"}}
        >
            <div className="d-flex justify-content-center" style={{paddingTop:"40vh"}}>
                <motion.div
                key="loadingSpinner"
                animate={{
                    scale:1
                }}
                initial={{
                    scale:100
                }}
                transition={{
                    duration:1
                }}
                exit={{
                    scale:100,
                    transition:{
                        duration:0.7,
                        delay:1
                    }
                }}
                style={{border:"2px solid black"}}
                >
                    <div className="spinner-border" style={{width: "3rem", height: "3rem", backgroundColor:"white"}} role="status">
                        <span className="visually-hidden">Loading...</span>
                    </div>
                </motion.div>
            </div>
        </motion.div>
    )
}

export default LoadingScreen;
