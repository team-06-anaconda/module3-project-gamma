import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import Login from "./accounts/Login.js";
import Logout from "./accounts/Logout";
import Nav from "./Nav.js";
import MainPage from "./MainPage.js";
import SignUp from "./accounts/SignUp";
import ProfilePage from "./profiles/ProfilePage";
import ResultsPage from "./trips/ResultsPage";
import EditProfile from "./profiles/EditProfile";
import MyTripsList from "./trips/MyTripsList";
import TripDetails from "./trips/TripDetails";
import EditTripForm from "./trips/EditTripForm";
import ErrorPage from "./ErrorPage";


const domain = /https:\/\/[^/]+/;
const basename = process.env.PUBLIC_URL.replace(domain, "");


function App() {
  return (
    <BrowserRouter basename={basename}>
      <Nav />
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="login" element={<Login />} />
        <Route path="logout" element={<Logout />} />
        <Route path="signup" element={<SignUp />} />
        <Route path="accounts">
          <Route path="profile">
            <Route index element={<ProfilePage />} />
            <Route path="edit" element={<EditProfile />} />
          </Route>
        </Route>
        <Route path="/trips">
          <Route path="mytrips" element={<MyTripsList />} />
          <Route path="results" element={<ResultsPage />} />
          <Route path=":id">
            <Route index element={<TripDetails />} />
            <Route path="edit" element={<EditTripForm />} />
          </Route>
        </Route>
        <Route path="*" element={<ErrorPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
