import { createSlice } from "@reduxjs/toolkit";


const initialState = {
    country: "",
    city: "",
}

const searchSlice = createSlice({
    name: "search",
    initialState,
    reducers: {
        SetSearch: (state, action) => {
            state.country = action.payload.country;
            state.city = action.payload.city;
        }
    }
})

export const { SetSearch } = searchSlice.actions;
export default searchSlice.reducer
