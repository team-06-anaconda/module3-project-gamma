import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";


export const tripOutAPI = createApi({
    reducerPath: "tripOutAPI",
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_API_HOST
    }),
    tagTypes: ["account", "profile", "trip", "location", "attractions", "search"],
    endpoints: builder => ({
        login: builder.mutation({
            query: data => ({
                url: "/token",
                body: data,
                method: "post",
                credentials: "include"
            }),
            invalidatesTags: ["account","profile", "trip"]
        }),
        logout: builder.mutation({
            query: () => ({
                url: "/token",
                method: "delete",
                credentials: "include"
            }),
            invalidatesTags: ["account", "profile", "trip"]
        }),
        signup: builder.mutation({
            query: data => ({
                url: "/accounts",
                body: data,
                method: "post",
                headers: {"Content-Type": "application/json"},
                credentials: "include"
            }),
            invalidatesTags: ["account","profile", "trip"]
        }),
        getAccount: builder.query({
            query: () => ({
                url: "/token",
                credentials: "include"
            }),
            transformResponse: (response) => (
                response? response.account : null
            ),
            providesTags: ["account"]
        }),
        getLocation: builder.query({
            query: (arg) => ({
                url: "/locations",
                credentials: "include",
                params: {
                    country: arg.country,
                    city: arg.city
                }
            }),
            providesTags: (result) =>
                result
                ? [{type:"location", id: `${result.country.name}, ${result.city.name}`}]
                : ["location"]
        }),
        getProfile: builder.query({
            query: () => ({
                url: "/profiles",
                credentials: "include"
            }),
            providesTags:["profile"]
        }),
        updateProfile: builder.mutation({
            query: (data) => ({
                url: "/profiles",
                credentials: "include",
                body: data,
                method: "put",
                headers: {
                    "Content-Type":"application/json"
                }
            }),
            invalidatesTags: ["profile"]
        }),
        getTrips: builder.query({
            query: () => ({
                url: "/trips",
                credentials: "include"
            }),
            providesTags: (results) =>
                results
                ? [...results.map(result => ({type: "trip", id: result.id})), {type:"trip", id:"LIST"}]
                : ["trip"],
            transformResponse: (response) => response.trips
        }),
        getTrip: builder.query({
            query: (args) => ({
                url: "/trips/" + args.tripId,
                credentials: "include",
            }),
            providesTags: (result) => [{type: "trip", id: result.id}]
        }),
        deleteTrip: builder.mutation({
            query: (tripId) => ({
                url: `/trips/${tripId}`,
                method: "delete",
                credentials: "include"
            }),
            invalidatesTags: [{type:"trip", id:"LIST"}]
        }),
        createTrip: builder.mutation({
            query: (data) => ({
                url: "/trips",
                method: "post",
                credentials: "include",
                body: data
            }),
            invalidatesTags: [{type:"trip", id:"LIST"}]
        }),
        updateTrip: builder.mutation({
            query: (data) => ({
                url: `/trips/${data.id}`,
                method: "put",
                credentials: "include",
                body: data
            }),
            invalidatesTags: (result) => [{type:"trip", id:result.id},{type:"trip", id:"LIST"}]
        }),
        filterAttractions: builder.query({
            query: (arg) => ({
                url: "/attractions",
                credentials: "include",
                params: {
                    city: arg.location.city,
                    country: arg.location.country,
                    query: arg.query? arg.query : "tours"
                }
            }),
            providesTags: ["attractions"]
        }),
        getWeather: builder.query({
            query: (arg) => ({
                url: "/weather",
                credentials: "include",
                params: {
                    country: arg.country,
                    city: arg.city
                }
            }),
            providesTags: ["weather"]
        }),
        getForecasts: builder.query({
            query: (arg) => ({
                url: "/forecast",
                credentials: "include",
                params: {
                    city: arg.city,
                    country: arg.country
                }
            }),
            providesTags: ["forecasts"]
        })
    })
})

export const { useLoginMutation, useLogoutMutation,
    useSignupMutation, useGetAccountQuery, useGetLocationQuery,
    useGetProfileQuery, useUpdateProfileMutation, useGetTripsQuery,
     useDeleteTripMutation, useGetTripQuery, useCreateTripMutation,
    useUpdateTripMutation, useFilterAttractionsQuery, useGetWeatherQuery, useGetForecastsQuery} = tripOutAPI;
