import { createSlice } from "@reduxjs/toolkit";


const initialState = {
    query: ""
}

const filterSlice = createSlice({
    name: "filter",
    initialState,
    reducers: {
        SetFilter: (state, action) => {
            state.query = action.payload.query;
        }
    }
})

export const { SetFilter } = filterSlice.actions;
export default filterSlice.reducer
