import { configureStore, combineReducers } from '@reduxjs/toolkit';
import {
  persistStore, persistReducer, FLUSH, REHYDRATE,
  PAUSE, PERSIST, PURGE, REGISTER
} from "redux-persist";
import storage from 'redux-persist/lib/storage';
import { tripOutAPI } from './tripOutAPI';
import searchReducer from './searchSlice';
import filterReducer from './filterSlice';


const persistConfig = {
  key: "root",
  storage,
  whitelist: ["search"],
}

const allCombinedReducers = combineReducers({
  tripOutAPI: tripOutAPI.reducer,
  search: searchReducer,
  filter: filterReducer
})

const persistReducers = persistReducer(persistConfig, allCombinedReducers)

export const store = configureStore({
  reducer: persistReducers,
  middleware: getDefaultMiddleware => getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
    }}).concat(tripOutAPI.middleware),
});

export const persistor = persistStore(store);
