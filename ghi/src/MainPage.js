import { useGetAccountQuery } from "./store/tripOutAPI";
import { Link } from "react-router-dom";
import Search from "./cards/SearchCard";
import { homePageLocations } from "./cards/static/locationDisplayList";
import StaticLocationCard from "./cards/StaticLocationCard";
import { useState } from 'react';
import "./accounts/css/home.css";
import video from "./accounts/css/loginbg.webm";


function MainPage() {
  const { data: account} = useGetAccountQuery();
  const [loading, setLoading] = useState(true)

  return (
    <>
		<div className="main"
			style={loading?{display:"none"}:{}}
			onLoad={() => setLoading(false)}
		>
			<video id="bgvid" autoPlay loop muted playsInline>
				<source src={video} type="video/webm" />
			</video>
			<div className="container">
				<div className="row d-flex justify-content-center align-items-center h-100 position">
					<div className="col-12 col-md-8 col-lg-6 col-xl-5 mt-5">
						<div className="bg-transparent text-dark">
							<div className="px-4 text-center">
								<div className="display-3 fw-bold mt-5">Trip Out!</div>
								<div className="card-body">
									<h5 className="card-title">
										Dare To Journey To The Unknown?
									</h5>
									{!account && (
										<Link to="/signup">
											<button type="button" className="btn btn-dark mt-3">
											Get Started
											</button>
										</Link>
									)}
									{!account && (
									<p className="mt-3">
										Already have an account? Login{" "}
										<Link to="/login" className="link-dark">here</Link>
									</p>)}
									{account && (
										<div className="mt-3">
											<Search/>
										</div>
									)}
									<div className="mt-3">
										<p>Our top trips for you to explore:</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div>
						<div className="card-group">
							{homePageLocations.map((location, index) => {
								return (
									<StaticLocationCard key={index} location={location}/>
								)
							})}
						</div>
					</div>
				</div>
			</div>
		</div>
    </>
  );
}

export default MainPage;
