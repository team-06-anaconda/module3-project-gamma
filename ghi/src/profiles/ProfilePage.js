import { useGetProfileQuery } from "../store/tripOutAPI";
import { NavLink } from "react-router-dom";
import "./css/profile.css";


function ProfilePage(){
    const {data: profile, isLoading} = useGetProfileQuery()
    const today = new Date();
    const bday = new Date(profile?.birthday? profile.birthday: null);

    const calculateAge = (birthdate) => {
        // Subtracting two different date objects results in a time difference in milliseconds
        const ageInMillis = today - birthdate;
        const ageDate = new Date(ageInMillis);
        // Subtract 1970 to account for epoch time, UTC stands for Coordinated Universal Time
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }
    const age = calculateAge(bday);

    if (isLoading){
        return (
            <div className="container-fluid" style={{paddingTop:"40vh"}}>
                <div className="d-flex justify-content-center">
                    <div className="spinner-border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </div>
                </div>
            </div>
        )
    }

    return (
        <div id="profile-wallpaper">
            <div className="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" id="profile-banner" style={{backgroundImage:`url(${profile.banner_image_url})`}}>
                <span className="mask bg-gradient-default opacity-8"/>
                <div className="container-fluid d-flex align-items-center">
                    <div className="row">
                        <div className="col-lg-10 col-md-10" id="banner-greeting">
                            <h1 className="display-2 text-white">Hello {profile.first_name? profile.first_name: "New User"}</h1>
                            <p className="text-white mt-0 mb-5">This is your profile page. You can see the various stats about your trips with TripOut</p>
                            <NavLink to="edit" className="btn btn-primary custom-btn">Edit Profile</NavLink>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container rounded-3" id="profile-content">
                <div className="profile-grid">
                    <div className="row">
                        <div className="col-xl-4 mb-3 mb-xl-4">
                            <div className="card justify-center custom-shadow-1 custom-card-bg">
                                <img src={profile.picture_url? profile.picture_url:"https://i.pinimg.com/550x/18/b9/ff/18b9ffb2a8a791d50213a9d595c4dd52.jpg"}
                                    alt="profile pic here"
                                    className="rounded-circle profile-pic"
                                    height="300px"
                                    width="300px" />
                                <p className="fs-4 profile-fullname">{profile.first_name? profile.first_name:"New"} {profile.last_name? profile.last_name:"User"}</p>
                                <p className="fs-5 profile-gender-text">{profile.preferred_gender? profile.preferred_gender:"Human"}</p>
                            </div>
                        </div>
                        <div className="col-xl-8 order-xl-2 mb-3 mb-xl-0">
                            <div className="card custom-shadow-2 custom-padding-1 custom-card-bg">
                                <h1>{profile.first_name? profile.first_name:"New"} {profile.last_name? profile.last_name:"User"}</h1>
                                <p>Nearest Airport: {profile.nearest_airport? profile.nearest_airport:"default Airport"}</p>
                                <p>Can Fly International: {profile.can_fly_international?"Yes":"No"}</p>
                                <p>Age: {age} </p>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="container ">
                            <div className="card custom-shadow-1 custom-padding-2 custom-card-bg">
                                <h3>About me</h3>
                                <p>{profile.about_me? profile.about_me:"Let's get to know you..."}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProfilePage;
