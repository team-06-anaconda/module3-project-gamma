import {useState, useEffect} from "react";
import { useGetProfileQuery, useUpdateProfileMutation } from "../store/tripOutAPI";
import { useNavigate } from "react-router-dom";
import "./css/profile.css";


function EditProfile(){
    const navigate = useNavigate()
    const {data: profile, error, isLoading} = useGetProfileQuery()
    const [updateProfile, result] = useUpdateProfileMutation()
    const [formData, setFormData] = useState({
        "nearest_airport": "",
        "can_fly_international": false,
        "first_name": "",
        "last_name": "",
        "birthday": "",
        "picture_url": "",
        "banner_image_url": "",
        "preferred_gender": "",
        "about_me": ""
    })

    const handleSubmit = async event => {
        event.preventDefault()
        updateProfile(formData)
    }

    const handleChange = event => {
        setFormData({
            ...formData,
            [event.target.id]: event.target.value
        })
    }

    const handleCheck = () => {
        if (formData.can_fly_international){
            setFormData({
                ...formData,
                can_fly_international: false
            })
        } else {
            setFormData({
                ...formData,
                can_fly_international: true
            })
        }
    }

    useEffect(() => {
        if (!isLoading && !error){
            setFormData({
                ...formData,
                can_fly_international: profile.can_fly_international,
                birthday: profile.birthday
            })
        }
        if (result.isSuccess){
            if (result.isSuccess){
                navigate("/accounts/profile")
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isLoading, result])


    return (
        <div className="container-fluid edit-profile-wallpaper">
            <div className="container">
                <div style={{paddingTop:"7em", paddingBottom:"4em"}}>
                    <div className="container rounded-3 mb-3" id="profile-content">
                        <div className="profile-grid">
                            <form onSubmit={handleSubmit}>
                                <div className="row">
                                    <div className="col-xl-3 mb-3 mb-xl-4">
                                        <div className="card justify-center" style={{backgroundColor:"#f9f9f9"}}>
                                            <img src={profile?.picture_url? profile.picture_url: "https://i.pinimg.com/550x/18/b9/ff/18b9ffb2a8a791d50213a9d595c4dd52.jpg"}
                                                alt="profile pic here"
                                                className="rounded-circle profile-pic"
                                                height="250px"
                                                width="250px" />
                                            <p className="fs-4 profile-fullname">{profile?.first_name? profile.first_name:"New"} {profile?.last_name? profile.last_name:"User"}</p>
                                            <p className="fs-5 profile-gender-text">{profile?.preferred_gender? profile.preferred_gender:"human"}</p>
                                        </div>
                                    </div>
                                    <div className="col-xl-5 mb-3 mb-xl-0">
                                        <div className="card custom-padding-1" style={{backgroundColor:"#f9f9f9"}}>
                                            <div className="form-group col">
                                                <h2 className="border-bottom border-dark">Profile Information</h2>
                                                <div className="row">
                                                    <div className="col mt-3">
                                                        <label className="" htmlFor="first_name">First Name</label>
                                                        <input className="form-control" onChange={handleChange} value={formData.first_name} placeholder={profile?.first_name} type="text" id="first_name"/>
                                                    </div>
                                                    <div className="col mt-3">
                                                        <label className="" htmlFor="last_name">Last Name</label>
                                                        <input className="form-control" onChange={handleChange} value={formData.last_name} placeholder={profile?.last_name} type="text" id="last_name"/>
                                                    </div>
                                                </div>
                                                <div className="mt-3">
                                                    <label className="" htmlFor="picture_url">Profile Picture URL</label>
                                                    <div className="col">
                                                        <input className="form-control" onChange={handleChange} value={formData.picture_url} placeholder={profile?.picture_url} type="text" id="picture_url" />
                                                    </div>
                                                </div>
                                                <div className="mt-3">
                                                    <label className="" htmlFor="banner_image_url">Banner Image URL</label>
                                                    <div className="col">
                                                        <input className="form-control" onChange={handleChange} value={formData.banner_image_url} placeholder={profile?.banner_image_url} type="text" id="banner_image_url" />
                                                    </div>
                                                </div>
                                                <div className="mt-3">
                                                    <label className="" htmlFor="birthday">Birthday</label>
                                                    <div className="col">
                                                        <input className="form-control" onChange={handleChange} value={formData.birthday}  type="date" id="birthday" />
                                                    </div>
                                                </div>
                                                <div className="mt-3">
                                                    <label className="" htmlFor="nearest_airport">Nearest Airport</label>
                                                    <div className="col">
                                                        <input className="form-control" onChange={handleChange} value={formData.nearest_airport} placeholder={profile?.nearest_airport} type="text" id="nearest_airport" />
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="mt-3 col">
                                                        <label className="" htmlFor="preferred_gender">Pronouns</label>
                                                        <div className="col">
                                                            <input className="form-control" onChange={handleChange} value={formData.preferred_gender} placeholder={profile?.preferred_gender} type="text" id="preferred_gender" />
                                                        </div>
                                                    </div>
                                                    <div className="col mt-1">
                                                        <div className="mt-5">
                                                            <label className="form-check-label" htmlFor="can_fly_international">Can Fly International</label>
                                                            <input className="form-check-input ps-5 ms-2" onChange={handleCheck} type="checkbox" id="can_fly_international" checked={formData.can_fly_international}/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="mt-3 mb-3 d-flex justify-content-center">
                                                <button className="btn btn-outline-secondary btn-lg" style={{color:"blue"}}>Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-4">
                                        <div className="card custom-padding-1" style={{backgroundColor:"#f9f9f9"}}>
                                            <h2 className="border-bottom border-dark">About me</h2>
                                            <div className="col mt-3">
                                                <textarea className="form-control" onChange={handleChange} value={formData.about_me} placeholder={profile?.about_me} id="about_me" rows="10"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default EditProfile;
