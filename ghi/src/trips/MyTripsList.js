import { useGetTripsQuery } from "../store/tripOutAPI";
import "./css/trips.css";
import {MyTripCard} from "../cards/_cards";


function MyTripsList() {
    const {data: trips, isLoading} = useGetTripsQuery()

    const sortedTrips = trips?[...trips].sort((a, b) => new Date(a.arrival) - new Date(b.arrival)):[]
    // if a-b is negative then a is smaller in milliseconds and comes before b.
    // if a-b is positive then b is smaller and comes first. sort function will put smaller values first

    return (
        <>
            <div className="container-fluid" id="mytrips-wallpaper">
                <div className="container">
                    <div>
                        <h1>My Trips</h1>
                    </div>
                    <div className="pb-3">
                        {!isLoading &&
                            sortedTrips.map((trip, index) => {
                                return (
                                    <MyTripCard key={index} index={index} trip={trip}/>
                                )
                        })}
                    </div>
                </div>
            </div>
        </>
    )
}

export default MyTripsList;
