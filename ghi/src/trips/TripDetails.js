import { useParams } from "react-router-dom";
import { useGetTripQuery } from "../store/tripOutAPI";
import { AnimatePresence } from "framer-motion";
import LoadingScreen from "../LoadingScreen";
import { Weather, AttractionCard } from "../cards/_cards";


function TripDetails () {
    const params = useParams()
    const {data: trip, isLoading} = useGetTripQuery({"tripId":params.id})

    return (
        <>
            <AnimatePresence>{ isLoading && <LoadingScreen/>} </AnimatePresence>
            { !isLoading &&
                <div className="container-fluid mountains layer2 pb-5 ">
                    <div className=" d-flex pt-3 pb-3 border-0 bg-transparent">
                        <div className="row">
                            <div className="col-auto pe-3">
                                <div className="d-flex">
                                    <h2 className="border-bottom border-dark p-1 flex-grow-1">
                                        {`${trip?.location.city.name}, ${trip?.location.country.name}`}
                                    </h2>
                                </div>
                                <div className="container bg-transparent border border-0 rounded" style={{backgroundColor:"white"}}>
                                    <div className="card bg-dark bg-opacity-50 text-white">
                                        <img className="card-img-top" src={trip?.location.picture_url.original} alt="city"/>
                                        <div className="card-body row">
                                            <div className="col-6">
                                                <h3>Local Information:</h3>
                                                <p className="card-text">Arrival: {trip?.arrival}</p>
                                                <p className="card-text">Departure: {trip?.departure}</p>
                                                <p className="card-text">Currency: {trip?.location.country.currency.name}</p>
                                                <p className="card-text">Capital: {trip?.location.country.capital}</p>
                                                <p className="card-text">Region: {trip?.location.country.region}</p>
                                                <p className="card-text">Population: {trip?.location.city.population.toLocaleString()}</p>
                                                <p className="card-text">Tourists: {trip?.location.country.tourists.toLocaleString()}</p>
                                                <p className="card-text">Closest Airport: {trip?.location.airport?.name}</p>
                                                <p className="card-text">Exchange Rate: {trip?.location.exchange_rate}</p>
                                                <p className="card-text">Time Zone: {trip?.location.airport?.timezone}</p>
                                            </div>
                                        <div className="col-6">
                                            <Weather/>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-4 mt-3 mt-lg-0 ps-3">
                            <div className="d-flex">
                                <h2 className="border-bottom border-dark p-1 flex-grow-1">
                                Attractions
                                </h2>
                            </div>
                            <div className="container bg-transparent border border-0 custom-results-height rounded " style={{backgroundColor:"white"}}>
                                <div className="card bg-transparent border-0" style={{height: "100vh", overflowX:"hidden", overFlowY:"auto"}}>
                                    {trip?.attraction.attractions.map((attraction, index) => {
                                        return (
                                            <AttractionCard attraction={attraction} key={index}/>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    )
}

export default TripDetails;
