import { useParams, useNavigate } from "react-router-dom"
import { useGetTripQuery } from "../store/tripOutAPI";
import { useUpdateTripMutation } from "../store/tripOutAPI";
import { useState, useEffect } from "react";
import {address_icon, phone_icon, score_icon, web_icon} from './css/icons/_icons.js';
import "./css/edit-trip.css";


function EditTripForm () {
    const params = useParams()
    const navigate = useNavigate()
    const {data: trip, error, isLoading} = useGetTripQuery({"tripId":params.id})
    const [updateTrip, results] = useUpdateTripMutation()
    const [ formData, setFormData ] = useState({
        arrival: "", departure: ""
    })

    const handleChange = event => {
        setFormData({
            ...formData,
            [event.target.id]: event.target.value
        })
    }

    const handleAddBookmark = event => {
        const updatedAttractions = JSON.parse(JSON.stringify(formData))
        updatedAttractions.attraction.attractions[event.target.value].bookmarked = true;
        setFormData(updatedAttractions)
    }

    const handleRemoveBookmark = event => {
        const updatedAttractions = JSON.parse(JSON.stringify(formData))
        updatedAttractions.attraction.attractions[event.target.value].bookmarked = false;
        setFormData(updatedAttractions)
    }

    const handleSubmit = event => {
        event.preventDefault()
        updateTrip(formData)
    }

    useEffect(() => {
        if (!isLoading && !error) {
            setFormData(trip)
        }

    }, [isLoading, error, trip])

    useEffect(() => {
        if (results){
            if (results.isSuccess){
                navigate("/trips/mytrips")
            } else if (results.isError) {
                navigate("/error")
            }
        }
    },[results, navigate])

    return (
        <div className="container-fluid edit-form-wallpaper pt-5 pb-5">
            <div className="container pt-3 pb-3 border border-dark" style={{backgroundColor:"white"}}>
                <div className="col border-bottom border-dark">
                    <h1>{trip?.location.city.name}, {trip?.location.country.name}</h1>
                </div>
                <div className="mt-3 p-2">
                    <form onSubmit={handleSubmit}>
                        <div className="row">
                            <div className="col">
                                <div>
                                    <label>Arrival</label>
                                    <input className="form-control" onChange={handleChange} id="arrival" value={formData?.arrival} type="date"/>
                                </div>
                            </div>
                            <div className="col">
                                <div>
                                    <label>Departure</label>
                                    <input className="form-control" onChange={handleChange} id="departure" value={formData?.departure} type="date"/>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="mt-2">
                                <button className="float-end me-2 btn btn-primary custom-edit-btn">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div className="row">
                    <div className="col-lg-6">
                        <p className="mb-0 fs-3">Bookmarked Attractions</p>
                        <div className="container border border-dark rounded custom-attraction-height pt-2" style={{backgroundColor:"white"}}>
                            {formData.attraction?.attractions
                                .filter(attraction => attraction.bookmarked)
                                .map((attraction, index) => {
                                return (
                                    <ul className="list-group list-group-flush" key={index}>
                                        <li className="list-group-item p-0 mt-2 mb-2 border border-dark rounded">
                                            <div className="card">
                                                <div className="card-body" style={{backgroundColor:"#f8f8f8"}}>
                                                    <h5 className="card-title text-decoration-underline">{attraction.poi.name}</h5>
                                                    <div>
                                                        <img className="mb-1" style={{marginLeft:"-7px"}} src={phone_icon} alt="Phone:" width="30em" height="30em"/>
                                                        <small className="card-text m-0 fs-6">{attraction.poi.phone? attraction.poi.phone: "none"}</small>
                                                    </div>
                                                    <div className="mb-1">
                                                        <img className="mb-1" src={web_icon} alt="URL:" height="18em" width="18em" />
                                                        <small className="card-text m-0 fs-6 ps-2">{attraction.poi.url? attraction.poi.url: "none"}</small>
                                                    </div>
                                                    <div className="mb-1">
                                                        <img className="mb-1" src={address_icon} alt="Address:" height="17em" width="17em"/>
                                                        <small className="card-text m-0 ms-2 fs-6">{attraction.address.freeformAddress}</small>
                                                    </div>
                                                    <div>
                                                        <img src={score_icon} alt="Score:" height="17em" width="17em"/>
                                                        <small className="card-text fs-6 ps-2">{(attraction.score * 10).toString().slice(0,4)}</small>
                                                    </div>
                                                    <button className="float-end custom-remove-btn" value={formData.attraction.attractions.indexOf(attraction)} onClick={handleRemoveBookmark}>Remove</button>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                )
                            })}
                        </div>
                    </div>
                    <div className="col-lg-6 mt-3 mt-lg-0">
                        <p className="mb-0 fs-3">Available Attractions</p>
                        <div className="container border border-dark rounded custom-attraction-height pt-2" style={{backgroundColor:"white"}}>
                            {formData.attraction?.attractions
                                .filter(attraction => !attraction.bookmarked)
                                .map((attraction, index) => {
                                return (
                                    <ul className="list-group list-group-flush" key={index}>
                                        <li className="list-group-item p-0 mt-2 mb-2 border border-dark rounded">
                                            <div className="card">
                                                <div className="card-body" style={{backgroundColor:"#f8f8f8"}}>
                                                    <h5 className="card-title text-decoration-underline">{attraction.poi.name}</h5>
                                                    <div>
                                                        <img className="mb-1" style={{marginLeft:"-7px"}} src={phone_icon} alt="Phone:" width="30em" height="30em"/>
                                                        <small className="card-text m-0 fs-6">{attraction.poi.phone? attraction.poi.phone: "none"}</small>
                                                    </div>
                                                    <div className="mb-1">
                                                        <img className="mb-1" src={web_icon} alt="URL:" height="18em" width="18em" />
                                                        <small className="card-text m-0 fs-6 ps-2">{attraction.poi.url? attraction.poi.url: "none"}</small>
                                                    </div>
                                                    <div className="mb-1">
                                                        <img className="mb-1" src={address_icon} alt="Address:" height="17em" width="17em"/>
                                                        <small className="card-text m-0 ms-2 fs-6">{attraction.address.freeformAddress}</small>
                                                    </div>
                                                    <div>
                                                        <img src={score_icon} alt="Score:" height="17em" width="17em"/>
                                                        <small className="card-text fs-6 ps-2">{(attraction.score * 10).toString().slice(0,4)}</small>
                                                    </div>
                                                    <button className="float-end custom-add-btn" value={formData.attraction.attractions.indexOf(attraction)} onClick={handleAddBookmark}>Add</button>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default EditTripForm;
