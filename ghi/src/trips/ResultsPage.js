import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { useGetLocationQuery } from "../store/tripOutAPI";
import { useSelector } from "react-redux";
import LoadingScreen from "../LoadingScreen";
import { AnimatePresence } from "framer-motion";
import "./css/trips.css";
import {
	Weather, AttractionCard, Favorite, Filter
} from "../cards/_cards";


export default function Results() {
    const location = useSelector((state) => state.search);
    const query = useSelector((state) => state.filter);
    const [pageLoaded, setPageLoaded] = useState(false)
    const [filtered, setFiltered] = useState([]);
    const navigate = useNavigate();
    const { data, isLoading, isError } = useGetLocationQuery(location);

    const getFilteredAttractions = async () => {
        const url = `${process.env.REACT_APP_API_HOST}/attractions?city=${
            location.city
        }&country=${location.country}&radius=32000&limit=51&query=${
            query.query? query.query: "tours"
        }`;
        const fetchOptions = {
            method: "GET",
            credentials: "include",
        };
        const response = await fetch(url, fetchOptions);
        if (response.ok) {
            const filtered = await response.json();
            setFiltered(filtered.attractions);
        } else {
			alert(
				"Sometimes the search query doesn't match anything in our database. Please try a different one."
			);
        }
    }

    useEffect(() => {
        if (isError) {
            navigate("/error");
        }
    }, [navigate, isError]);

    useEffect(() => {
      	getFilteredAttractions();
		// eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query])

    return (
        <>
            <AnimatePresence>{!pageLoaded && <LoadingScreen />}</AnimatePresence>
            {!isLoading &&
            <div className="container-fluid mountains layer1 pb-5">
                <div className="row">
                    <div className="col-xl-8 col-lg-7 pt-3 pb-3 border-0 bg-transparent">
                        <div className="row border-bottom border-dark mb-1 mx-1" name="heading">
                            <div className="col ps-0" name="title">
                                <h2 className="pt-1 pe-1 mb-1">
                                    {`${data?.city.name}, ${data?.country.name}`}
                                </h2>
                            </div>
                            <div className="col" name="favorite-button">
                                <div className="mt-1 pe-0 me-0 float-end">
                                    <Favorite />
                                </div>
                            </div>
                        </div>
                        <div className="row" name="content">
                            <div className="container">
                                <div className="card bg-dark bg-opacity-25 mx-2 text-white">
                                    <img
                                        src={data?.picture_url.original}
                                        className="card-img-top"
                                        alt="..."
                                        onLoad={() => setPageLoaded(true)}
                                    />
                                    <div className="card-body row">
                                        <div className="col-xl-6">
                                            <p className="card-text">
                                                Population: {data?.city.population.toLocaleString()}
                                            </p>
                                            <p className="card-text">
                                                Nearest Airport: {data?.airport?.name}
                                            </p>
                                            {data.exchange_rate.includes("USD") ? null : (
                                                <p className="card-text">
                                                    Exchange Rate: {data?.exchange_rate}
                                                </p>
                                            )}
                                            <p className="card-text">
                                                Annual Tourists: {data?.country.tourists.toLocaleString()}
                                            </p>
                                            <p className="card-text">
                                                Nearby Attractions: {(filtered.length > 50)? "50+" : filtered.length}
                                            </p>
                                        </div>
                                        <div className="col-xl-6 mt-3 mt-xl-0">
                                            <Weather />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-4 col-lg-5 mt-3 pe-4">
                        <div className="row border-bottom border-dark mb-1 mx-1" name="heading">
                            <div className="col ps-0" name="title">
                                <h2 className="pt-1 pe-1 mb-1">
									Attractions
                                </h2>
                            </div>
                            <div className="col" name="favorite-button">
                                <div className="float-end mt-xl-1 mt-0">
									<Filter />
								</div>
                            </div>
                        </div>
                        <div className="row" name="content">
                            <div className="container bg-transparent border border-0 attractions-box custom-results-height pe-0 rounded"
									style={{ backgroundColor: "white" }}
                            >
                                <div className="card bg-transparent border-0">
                                    <div>
                                        {filtered?.map((attraction, index) => {
                                            return (
                                                <AttractionCard attraction={attraction} key={index} />
                                            );
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            }
        </>
    );
};
