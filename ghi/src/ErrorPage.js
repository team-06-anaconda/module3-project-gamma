import { Link } from "react-router-dom";
function ErrorPage() {
    return (
        <>
        <div className="container p-5">
            <div className="card text-bg-dark">
                <img src={require("./404page.gif")} className="card-img" alt="..."/>
                <div className="container card-img-overlay">
                    <div className="d-flex justify-content-center">
                       <div className="bg-dark rounded p-2 border border-white bg-opacity-75">
                        <h1 className="card-title d-flex justify-content-center">OH NO! ERROR 404</h1>
                        <p className="card-text d-flex justify-content-center">It looks like you are trying to access a page that doesn't exist.</p>
                        <p className="card-text d-flex justify-content-center">Make sure to use correct query parameters when searching. </p>
                        <Link to="/"><p className="card-text d-flex justify-content-center"><small>Please return home</small></p></Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>
    )
}

export default ErrorPage;
