# Trip Out

- Blake Callahan
- Marce Isidro
- Aidan Dixon
- Brooks Burris


## Design

- [API design](docs/api_design.md)
- [Data model](docs/data-model.md)
- [GHI](docs/ghi.md)
- [Integrations](docs/integrations.md)

## Intended market

We are targeting general consumers who like to travel and keep itineraries of their trips.

## Functionality

- Guests can create a profile and customize with their information for a better user experience
- Users can search locations by city and country to get useful local information including current weather for when they travel to their destination.
- Attractions can be filtered via query to give a sense of what there is to do nearby.
- Users can 'add' searched locations and queried attractions with an arrival and departure dates to their 'My Trips' page for user accessibility.
- Users can edit their added trips to change arrival and departure dates as well as add saved attractions to their itinerary.
- Users can delete a trip.



## Project Initialization

To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Clone the repository down to your local machine
2. CD into the new project directory
3. sign up for following keys:
    ATTRACTION_API_KEY - https://developer.tomtom.com/
    NINJA_KEY - https://api-ninjas.com/
    OPEN_WEATHER_API_KEY - https://openweathermap.org/api
    FORECAST_KEY - https://developer.accuweather.com/
    PEXELS_KEY - https://www.pexels.com/
4. Create .env to house api keys:
    REACT_APP_API_HOST=http://localhost:8000
    SIGNING_KEY = "some 16 digit alphanumeric string"
    OPEN_WEATHER_API_KEY = "your key here"
    ATTRACTION_API_KEY = "your key here"
    NINJA_KEY = "your key here"
    PEXELS_KEY = "your key here"
    FORECAST_KEY = "your key here"
5. Run `docker volume create trip-out-db`
6. Run `docker compose build`
7. Run `docker compose up`
8. Exit the container's CLI, and trip out with Trip Out!
